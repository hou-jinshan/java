# linux安装问题总结

## 一、新安装的linux系统没有网络

linux打开网络，打开网关

```java
# 可以查看ifcfg-***为多少，不是lo的那个，那个是本机
cd /etc/sysconfig/network-scripts/ 
ls 
```

如图

![在这里插入图片描述](https://img-blog.csdnimg.cn/cc92c5bacb0c48a1b1288d152b7bee56.png)

编辑网络配置

\#(根据查看前面的ifcfg-en***）回车
\#进入网卡后注意 onboot，改成yes

```
vi ifcfg-enpls0
```

如图

![在这里插入图片描述](https://img-blog.csdnimg.cn/9a7e24493d25476fb8fc0a1a05dd6e8e.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBAd2VpeGluXzUyNjgwMTY4,size_14,color_FFFFFF,t_70,g_se,x_16#pic_center)

![在这里插入图片描述](https://img-blog.csdnimg.cn/9b17074213a84c3e9a3aa9144f549b1c.png)

```
#重启网络(二选一)

service network restart

/etc/init.d/network  restart
```

```
修改完了后，重启网络服务：systemctl restart network
再次查看网卡配置：ip a
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/51732cc7594a4314823f84055214a407.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBAd2VpeGluXzUyNjgwMTY4,size_20,color_FFFFFF,t_70,g_se,x_16#pic_center)

![在这里插入图片描述](https://img-blog.csdnimg.cn/09945d58ef614700bdba18d42011454a.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBAd2VpeGluXzUyNjgwMTY4,size_18,color_FFFFFF,t_70,g_se,x_16)

使用 curl www.baidu.com 测试是否能获取到信息。