# docker使用问题

## 一、出现Got permission denied while trying to connect to the Docker daemon socket at unix: ///var/run的解决方法

```
Got permission denied while trying to connect to the Docker daemon socket at unix:
///var/run/docker.sock: Get "http://%2Fvar%2Frun%2Fdocker.sock/v1.24/containers/json": dial unix /var/run/docker.sock: connect: permission denied

```

截图如下：

![在这里插入图片描述](https://img-blog.csdnimg.cn/8692784824d94bac81be6ca9acc3868f.png)

# 临时方法

将其docker的用户添加到sudo的用户中即可
但是这种方法打开另外的终端就会失效，也是一个缺陷点

```
sudo groupadd docker

sudo gpasswd -a gaokaoli docker
具体命令参数如下：gpasswd -a user_name group_name
不推荐使用这个， usermod -G group_name user_name 这个命令可以添加一个用户到指定的组，但是以前添加的组就会清空掉

newgrp docker #更新用户组
```

# 永久方法

```
执行 sudo chmod a+rw /var/run/docker.sock
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/b56c5ac11e384ec197d63b9fa6e31856.png)