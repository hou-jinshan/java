

# docker学习

docker官方网站 https://www.docker.com/

dockerhub全球最大的docker镜像网站 https://hub.docker.com

centos手动安装说明 https://docs.docker.com/engine/install/centos/





## 一、概论

docker包含：镜像、容器、仓库



## 二、安装

### 1、卸载旧版本

```
sudo yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-engine
```

### 2、切换仓库

不要使用官方网站的，更换成阿里云的镜像

```
sudo yum install -y yum-utils
sudo yum-config-manager \
    --add-repo \
    https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
```

参考网站：

https://developer.aliyun.com/mirror/docker-ce?spm=a2c6h.13651102.0.0.5c3b1b11rWyiiV

### 3.安装引擎

```
sudo yum install docker-ce docker-ce-cli containerd.io docker-compose-plugin
```

### 4.启动docker

```
sudo systemctl start docker
```

### 5.run镜像

```
sudo docker run hello-world
```

### 6.卸载

卸载docker

```
sudo yum remove docker-ce docker-ce-cli containerd.io docker-compose-plugin
```

删除配置文件

```
sudo rm -rf /var/lib/docker
sudo rm -rf /var/lib/containerd
```

### （重要）添加加速器

例如阿里云

登陆阿里云，进入弹性计算，容器镜像，可免费申请个人版镜像加速器

获取到加速器地址

```
https://11w8d6s3.mirror.aliyuncs.com
```

```
Ubuntu

1.安装／升级Docker客户端

推荐安装1.10.0以上版本的Docker客户端，参考文档docker-ce

2. 配置镜像加速器

sudo mkdir -p /etc/docker
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://11w8d6s3.mirror.aliyuncs.com"]
}
EOF
sudo systemctl daemon-reload
sudo systemctl restart docker
```

```
CentOS

1.安装／升级Docker客户端

推荐安装1.10.0以上版本的Docker客户端，参考文档docker-ce

2. 配置镜像加速器

sudo mkdir -p /etc/docker
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://11w8d6s3.mirror.aliyuncs.com"]
}
EOF
sudo systemctl daemon-reload
sudo systemctl restart docker
```

网站:https://cr.console.aliyun.com/cn-hangzhou/instances/mirrors

### 7、总结

1、为什么docker比VM虚拟机块

(1)docker有着比虚拟机更少的抽象层

由于docker不需要Hypervisor(虚拟机)实现硬件资源虚拟化，运行在dokcer容器上的程序直接使用

的都是实际物理机的硬件资源。因此在cpu、内存利用率上docker将会在效率上有明显优势。

(2)docekr利用的是宿主机的内核，而不需要加载操作系统OS内核

当新建一个容器时，docker不需要和虚拟机一样重新加载一个操作系统内核。进而避免引寻、加载操作系统内核返回等比较费时费资源的过程，当新建一个虚拟机时，虚拟机软件需要加载OS，返回新建过程是分钟级别的而docker由于直接利用宿主机的操作系统则省略了返回过程，因此新建一个docker容器只需要几秒钟。



## 三、常用命令

```
启动docker: systemctl start docker

停止docekr: systemctl stop docker

重启docker: systemctl restart docker

查看docker状态: systemctl status docker

开机启动: systemctl enable docker

查看docker概要信息:docker info

查看docker总体帮助文档: docker --help

查看docker命令帮助文档: docker 具体命令 --help
```

