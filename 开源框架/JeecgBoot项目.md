# 										JeecgBoot项目

## 项目git地址

后端源码

https://gitee.com/jeecg/jeecg-boot.git

前端vue2版

https://gitee.com/jeecg/ant-design-vue-jeecg.git

官网

http://www.jeecg.com/

## 后端环境配置启动

### （1）初始化数据库 (要求 mysql5.7+)

> 执行Sql脚本： jeecg-boot/db/jeecgboot-mysql-5.7.sql
> 脚本工作：自动创建库`jeecg-boot`, 并初始化数据 。

### （2）修改项目配置 (数据库、redis等)

```
配置文件： jeecg-module-system/jeecg-system-start/src/main/resources/application-dev.yml
```

a. 数据库配置(连接和账号密码)

b. Redis配置（配置redis的host和port）

### （3）Maven私服配置

- 找到 maven老家 conf/settings.xml，
  在标签内增加下面方式的阿里云maven镜像（删除自己的镜像配置）， 最终结果见下面：

```
<mirrors>
       <mirror>
            <id>nexus-aliyun</id>
            <mirrorOf>*,!jeecg,!jeecg-snapshots,!getui-nexus</mirrorOf>
            <name>Nexus aliyun</name>
            <url>http://maven.aliyun.com/nexus/content/groups/public</url>
        </mirror> 
 </mirrors>
```

- 此配置重点在这句话`<mirrorOf>*,!jeecg,!jeecg-snapshots</mirrorOf>`
  如果不加这句话，默认所有的依赖都会去阿里云仓库下载，加上后jeecg的依赖包就可以从jeecg私服下载了。

### （4）启动项目&访问

以上配置完成后，即可启动后台项目

- 找到类 `jeecg-module-system/jeecg-system-start/src/main/java/org/jeecg/JeecgSystemApplication.java` 右键执行启动。
- 通过 `http://localhost:8080/jeecg-boot/doc.html` 访问后台项目的swagger地址

## 前端vue2环境配置启动

### （1）安装node.js

**验证**

```
# 出现相应npm版本即可
npm -v
# 出现相应node版本即可
node -v
```

### （2）安装yarn

```
# 全局安装yarn
npm i -g yarn
# 验证
yarn -v # 出现对应版本号即代表安装成功
```

### （3）切换到ant-design-vue-jeecg文件夹下

```
# 下载依赖
yarn install

# 启动
yarn run serve
```

### （4）接口地址配置

.env.development

```
NODE_ENV=development
VUE_APP_API_BASE_URL=http://localhost:8080/jeecg-boot
```

## 代码生成器

1、在线开发——>Online表单开发

2、新增（切记提供的字段**不能修改不能删除**，只能在后续添加）

![image-20221025203608262](C:\Users\22758\AppData\Roaming\Typora\typora-user-images\image-20221025203608262.png)

3、更多——>同步数据库

4、代码生成

![image-20221025204353986](C:\Users\22758\AppData\Roaming\Typora\typora-user-images\image-20221025204353986.png)

将生成到本地的代码对应复制至IDEA一下目录对应的包下

![image-20221025204214899](C:\Users\22758\AppData\Roaming\Typora\typora-user-images\image-20221025204214899.png)

vue文件夹指vue2

![image-20221025204432611](C:\Users\22758\AppData\Roaming\Typora\typora-user-images\image-20221025204432611.png)

vue文件夹内容

![image-20221025204612672](C:\Users\22758\AppData\Roaming\Typora\typora-user-images\image-20221025204612672.png)

将modules文件夹内容复制至vue项目system/modules

将***.vue复制至vue项目system

在数据库执行***.sql脚本

![image-20221025204745060](C:\Users\22758\AppData\Roaming\Typora\typora-user-images\image-20221025204745060.png)

5、重启项目

6、系统管理——>菜单管理——>新增

![image-20221025205143988](C:\Users\22758\AppData\Roaming\Typora\typora-user-images\image-20221025205143988.png)

7、角色管理——>授权

![image-20221025205218182](C:\Users\22758\AppData\Roaming\Typora\typora-user-images\image-20221025205218182.png)8、刷新页面