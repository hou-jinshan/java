# flex属性详解

## 多用于移动端

## 1、flex涉及的概念

Flex的基本作用就是让布局变的更简单，比如“垂直居中”等，当然不止这个，要说清楚flex有什么作用首先要了解一些概念。 
采用Flex布局的元素，称为Flex容器（flex container），简称”容器”。它的所有子元素自动成为容器成员，称为Flex项目（flex item），简称”项目”。 

![flex](https://img-blog.csdn.net/20160706144733310)

容器默认存在两根轴：水平的主轴（main axis）和垂直的交叉轴（cross axis）。主轴的开始位置（与边框的交叉点）叫做main start，结束位置叫做main end；交叉轴的开始位置叫做cross start，结束位置叫做cross end。 
项目默认沿主轴排列。单个项目占据的主轴空间叫做main size，占据的交叉轴空间叫做cross size。 
主要记住 “容器”、“项目”、“主轴（横轴）”和“交叉轴（纵轴）”的意思和指向就行。

## 2、容器的属性

### 2.1 flex-direction

flex-direction 决定主轴的方向（即项目的排列方向）。它有4个可能的值 ： 
row（默认值）：主轴为水平方向，起点在容器的左端。 

![row](https://img-blog.csdn.net/20160706145001734)

row-reverse：主轴为水平方向，起点在容器的右端。 

![row-reverse](https://img-blog.csdn.net/20160706145010171)

column：主轴为垂直方向，起点在容器的上沿。 

![column](https://img-blog.csdn.net/20160706145020203)

column-reverse：主轴为垂直方向，起点在容器的下沿。 

![column-reverse](https://img-blog.csdn.net/20160706145029593)

### 2.2 flex-wrap

默认情况下容器里变得额所有项目都排在一条线上，flex-wrap定义如果一行排不下 如何换行。它可能的值有三个： 
nowrap（默认）：不换行。 

![nowrap](https://img-blog.csdn.net/20160706145152205)

wrap：换行，第一行在上方。 

![wrap](https://img-blog.csdn.net/20160706145209518)

wrap-reverse：换行，第一行在下方。

![wrap-reverse](https://img-blog.csdn.net/20160706145223581)

### 3.3 flex-flow

flex-flow属性是flex-direction属性和flex-wrap属性的简写形式，默认值为row nowrap。

### 3.4 justify-content 

justify-content 定义了项目在主轴上的对齐方式。它可能的值有5个： 

flex-start:向主轴的起始位置对齐，也就是从主轴的起始位置开始排列。如果使用flex-direction 属性改变的主轴的方向，那项目对应的排列方式也会变。

```css
.container{
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
}
```

![flex-start](https://img-blog.csdn.net/20160706145357532)

```css
.container{
    display: flex;
    flex-direction: row-reverse;
    justify-content: flex-start;
}
```

![flex-start](https://img-blog.csdn.net/20160706145414970)

flex-end：向主轴结束位置对齐，也就是从主轴结束的位置开始排列。和flex-start一样也和flex-direction有关。

```css
.container{
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
}
```

![flex-end](https://img-blog.csdn.net/20160706145442552)

center： 居中

```css
.container{
    display: flex;
    flex-direction: row;
    justify-content: center;
}
```

![center](https://img-blog.csdn.net/20160706145459256)

space-between：如果有两个以上的项目，则容器主轴的开始和结束位置各一个，其他的项目均匀排列，项目之间的间隔相等。排列顺序同样和flex-direction有关。如果只有两个项目则一边一个。如果只有一个项目则只在容器主轴的开始位置排列 

![这里写图片描述](https://img-blog.csdn.net/20160706145610773)

![这里写图片描述](https://img-blog.csdn.net/20160706145620429)

![这里写图片描述](https://img-blog.csdn.net/20160706145629674)

space-around：每个项目两侧的间隔相等。所以，项目之间的间隔比项目与边框的间隔大一倍。排列顺序同样和flex-direction有关。如果只有一个项目则排列在中间。 

![这里写图片描述](https://img-blog.csdn.net/20160706145714924)

![这里写图片描述](https://img-blog.csdn.net/20160706145722680)

![这里写图片描述](https://img-blog.csdn.net/20160706145730384)

### 3.5 align-items属性

align-items属性定义项目在交叉轴（纵轴）上如何对齐。它可能取5个值。具体的对齐方式与交叉轴的方向有关，下面假设交叉轴从上到下。 
flex-start：交叉轴的起点对齐。 

![这里写图片描述](https://img-blog.csdn.net/20160706145816049)

flex-end：交叉轴的终点对齐。

![这里写图片描述](https://img-blog.csdn.net/20160706145824174)

center：交叉轴的中点对齐。 

![这里写图片描述](https://img-blog.csdn.net/20160706145831385)

baseline: 项目的第一行文字的基线对齐。 

![这里写图片描述](https://img-blog.csdn.net/20160706145839369)

stretch（默认值）：如果项目未设置高度或设为auto，将占满整个容器的高度。

![这里写图片描述](https://img-blog.csdn.net/20160706145846698)

### 3.6 align-content属性

align-content属性定义了多根轴线（多行）的对齐方式。如果项目只有一根轴线（一行），该属性不起作用。 
如果flex-direction的值是column，则该属性定义了多列的对齐方式。如果项目只有一列，该属性不起左右。 
stretch（默认值）：多行占满整个交叉轴。 

![这里写图片描述](https://img-blog.csdn.net/20160706145920628)

flex-start：与交叉轴的起点对齐。 

![这里写图片描述](https://img-blog.csdn.net/20160706145927800)

flex-end：与交叉轴的终点对齐。 

![这里写图片描述](https://img-blog.csdn.net/20160706145938190)	

center：与交叉轴的中点对齐。 	

![这里写图片描述](https://img-blog.csdn.net/20160706145946965)

space-between：与交叉轴两端对齐，轴线之间的间隔平均分布。 

![这里写图片描述](https://img-blog.csdn.net/20160706145956347)

space-around：每根轴线两侧的间隔都相等。所以，轴线之间的间隔比轴线与边框的间隔大一倍。 

![这里写图片描述](https://img-blog.csdn.net/20160706150009847)

## 四、项目的属性

### 4.1 order属性

```css
.item {
  order: <integer>;
}
```

order属性定义项目的排列顺序。数值越小，排列越靠前，默认为0。

### 4.2 flex-grow属性

```css
 
.item {
  flex-grow: <number>; /* default 0 */
}
```

flex-grow属性定义项目的放大比例，默认为0。 

![这里写图片描述](https://img-blog.csdn.net/20160706150152593)

如果所有的item 的flex-grow的值都是一样的话那就是以item 的width为最小值平均分配主轴上的宽度。如果item没有设置width则所有的item平分主轴上的剩余宽度（多余空间）。 
如果item的flex-grow的值不一样，那就是根据对应的比例来分配主轴上的剩余宽度（多余空间）。同样是以item设置的width为最小值。 
如果item设置的max-width则放大的宽度不会超过该值。

### 4.3 flex-shrink属性

flex-shrink属性定义了项目的缩小比例，默认为1，即如果空间不足，该项目将缩小。

```css
.item {
  flex-shrink: <number>; /* default 1 */
}
```

![这里写图片描述](https://img-blog.csdn.net/20160706150209531)

如果所有项目的flex-shrink属性都为1，当空间不足时，都将等比例缩小。如果一个项目的flex-shrink属性为0，其他项目都为1，则空间不足时，前者不缩小。 
如果container容器设置的flex-wrap则不存在空间不足的情况，如果超过会自动换行。所以这时候设置flex-shrink也是不起作用的。 
负值对该属性无效。

### 4.4 flex-basis属性

flex-basis属性定义了在分配多余空间之前，项目占据的主轴空间（main size）。浏览器根据这个属性，计算主轴是否有多余空间。它的默认值为auto，即项目的本来大小。

```css
.item {
  flex-basis: <length>|auto; /* default auto */
}
```

它可以设为跟width或height属性一样的值（比如350px），则项目将占据固定空间。

### 4.5 flex属性

flex属性是flex-grow, flex-shrink 和 flex-basis的简写，默认值为0 1 auto。后两个属性可选。

```css
.item {
  flex: none | [ <'flex-grow'> <'flex-shrink'>? || <'flex-basis'> ]
}
```

该属性有两个快捷值：auto (1 1 auto) 和 none (0 0 auto)。 
建议优先使用这个属性，而不是单独写三个分离的属性，因为浏览器会推算相关值。 
如果flex-basis的总和加起来大于父级宽度，子级被压缩，最后的选择是flex-shrink来进行压缩计算

```
加权值 = son1 + son2 + …. + sonN；
```

那么压缩后的计算公式就是

```
压缩的宽度 w = (子元素flex-basis值 * (flex-shrink)/加权值) * 溢出值
```

如果flex-basis的总和小于父级宽度，剩余的宽度将根据flex-grow值的总和进行百分比；

```
扩张的宽度 w = (子元素flex-grow值 /所有子元素flex-grow的总和) * 剩余值
```

### 4.6 align-self属性

align-self属性允许单个项目有与其他项目不一样的对齐方式，可覆盖align-items属性。默认值为auto，表示继承父元素的align-items属性，如果没有父元素，则等同于stretch。

```css
.item {
  align-self: auto | flex-start | flex-end | center | baseline | stretch;
}
```

