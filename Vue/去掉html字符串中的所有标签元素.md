# 去掉html字符串中的所有标签元素 

 // 去掉html字符串中的所有标签元素

  delHtmlTag(str) {

   return str.replace(/<[^>]+>/g, "");

  },