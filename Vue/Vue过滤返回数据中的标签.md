# Vue过滤返回数据中的标签

```vue
 filters: {

  // 当标题字数超出时，超出部分显示’...‘。此处限制超出8位即触发隐藏效果

  ellipsis(value) {

   if (!value) return "";

   if (value.length > 8) {

​    return value.slice(0, 3) + "...";

   }

   return value;

  },

 },
```

