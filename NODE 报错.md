# NODE 报错



## 一、info it worked if it ends with ok

1、先卸载之前版本的node-sass
npm uninstall node-sass

2、卸载后安装4.0.0版本
npm install node-sass@4.14.1

安装的时候可能会失败，有可能是安装的淘宝镜像的问题

3、重新安装下淘宝镜像
npm install -g cnpm --registry=https://registry.npm.taobao.org

4、重装成功后，直接执行
npm install node-sass@4.14.1

