# MySql函数

## 1. 自定义函数

语法：

```
CREATE FUNCTION <函数名> ( [ <参数1> <参数1类型> ],[ <参数2> <参数2类型>])
RETURNS <返回值类型>
BEGIN
 <函数体>
 RETURN (<返回值>);
END
```

示例：

```mysql
CREATE FUNCTION getNthHighestSalary(N INT) RETURNS INT
BEGIN
	#声明一个变量a
	declare a int default N-1;
	RETURN (
      # Write your MySQL query statement below.
      select distinct Salary from Employee order by Salary desc limit a,1
	);
END
```



## 2. 函数自定义局部变量和赋值

语法

```
DECLARE <变量名> <变量类型> DEFAULT <默认值>;
SET <变量名>=<值>;
```

示例：

```mysql
CREATE FUNCTION getNthHighestSalary(N INT) RETURNS INT
BEGIN
	#声明一个变量a
	declare a int default 0;
	#给变量赋值
	set a=N-1;
	RETURN a；
END
```

## 3. 调用自定义函数

```
select getNthHighestSalary(2)
```

## 4. 删除自定义函数

语法：

```
DROP FUNCTION [ IF EXISTS ] <自定义函数名>
```

## 5.在mysql中创建自定义函数报错

出现这个错误的是因为开启了bin-log, 我们就必须指定函数是否是
1 DETERMINISTIC 不确定的
2 NO SQL 没有SQl语句，当然也不会修改数据
3 READS SQL DATA 只是读取数据，当然也不会修改数据
4 MODIFIES SQL DATA 要修改数据
5 CONTAINS SQL 包含了SQL语句
其中在function里面，只有 DETERMINISTIC, NO SQL 和 READS SQL DATA 被支持。如果我们开启了 bin-log, 我们就必须为我们的function指定一个参数。
在MySQL中创建函数时出现这种错误的解决方法：
set global log_bin_trust_function_creators=TRUE;

创建示例：

```mysql
create function getNthHighestSalarys(N int)returns int
READS SQL DATA
begin
	return(
		select distinct Salary from Employee order by Salary desc limit N,1
	);
end
```

