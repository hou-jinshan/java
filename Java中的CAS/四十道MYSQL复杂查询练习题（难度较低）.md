

# 四十道MYSQL复杂查询练习题（难度较低）

```mysql
-- 建库
CREATE DATABASE `emp`;
-- 打开库
USE emp;
-- 建dept表
CREATE TABLE `dept`( `deptno` INT(2) NOT NULL, `dname` VARCHAR(14), `loc` VARCHAR(13), CONSTRAINT pk_dept PRIMARY KEY(deptno) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- 键emp表
CREATE TABLE `emp` ( `empno` int(4) NOT NULL PRIMARY KEY, `ename` VARCHAR(10), `job` VARCHAR(9), `mgr` int(4), `hiredate` DATE, `sal` float(7,2), `comm` float(7,2), `deptno` int(2), CONSTRAINT fk_deptno FOREIGN KEY(deptno) REFERENCES dept(deptno) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- 建salgrade表
CREATE TABLE `salgrade` ( `grade` int, `losal` int, `hisal` int ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- 插入数据
INSERT INTO dept VALUES (10,'ACCOUNTING','NEW YORK'); 
INSERT INTO dept VALUES (20,'RESEARCH','DALLAS');
INSERT INTO dept VALUES (30,'SALES','CHICAGO'); 
INSERT INTO dept VALUES (40,'OPERATIONS','BOSTON');
INSERT INTO EMP VALUES (7369,'SMITH','CLERK',7902,'1980-12-17',800,NULL,20); 
INSERT INTO EMP VALUES (7499,'ALLEN','SALESMAN',7698,'1981-02-20',1600,300,30); 
INSERT INTO EMP VALUES (7521,'WARD','SALESMAN',7698,'1981-02-22',1250,500,30); 
INSERT INTO EMP VALUES (7566,'JONES','MANAGER',7839,'1981-04-02',2975,NULL,20);
INSERT INTO EMP VALUES (7654,'MARTIN','SALESMAN',7698,'1981-09-28',1250,1400,30); 
INSERT INTO EMP VALUES (7698,'BLAKE','MANAGER',7839,'1981-05-01',2850,NULL,30); 
INSERT INTO EMP VALUES (7782,'CLARK','MANAGER',7839,'1981-06-09',2450,NULL,10); 
INSERT INTO EMP VALUES (7788,'SCOTT','ANALYST',7566,'1987-07-13',3000,NULL,20); 
INSERT INTO EMP VALUES (7839,'KING','PRESIDENT',NULL,'1981-11-07',5000,NULL,10); 
INSERT INTO EMP VALUES(7844,'TURNER','SALESMAN',7698,'1981-09-08',1500,0,30); 
INSERT INTO EMP VALUES (7876,'ADAMS','CLERK',7788,'1987-07-13',1100,NULL,20); 
INSERT INTO EMP VALUES (7900,'JAMES','CLERK',7698,'1981-12-03',950,NULL,30); 
INSERT INTO EMP VALUES (7902,'FORD','ANALYST',7566,'1981-12-03',3000,NULL,20); 
INSERT INTO EMP VALUES (7934,'MILLER','CLERK',7782,'1982-01-23',1300,NULL,10);
INSERT INTO SALGRADE VALUES (1,700,1200); 
INSERT INTO SALGRADE VALUES (2,1201,1400); 
INSERT INTO SALGRADE VALUES (3,1401,2000); 
INSERT INTO SALGRADE VALUES (4,2001,3000); 
INSERT INTO SALGRADE VALUES (5,3001,9999);
```

**dept表：**

![在这里插入图片描述](https://img-blog.csdnimg.cn/a9609ff967014b21957c98a5ab7bf19a.png)

**emp表：**

![在这里插入图片描述](https://img-blog.csdnimg.cn/942f788d605f445d8beaa42640a45027.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA5qGD6Iqx6ZSu56We,size_20,color_FFFFFF,t_70,g_se,x_16)

**salgrade表：**

![在这里插入图片描述](https://img-blog.csdnimg.cn/71d5c025a2f64ebbb5436cfe7dda4834.png)

## 字段描述

```
雇员表：
记录了一个雇员的基本信息 EMP（雇员表）
NO -----------字段 ---------------类型-----------------描述
1 ---------EMPNO ------ NUMBER(4)---------- 雇员编号
2 ---------ENAME----- VARCHAR2(10) ----表示雇员姓名
3 ----------JOB ----------VARCHAR2(9) -----表示工作职位
4--------- MGR ---------NUMBER(4) 表示一个雇员的领导编号
5 ------HIREDATE ---------DATE------------ 表示雇佣日期
6 -------SAL ------------NUMBER(7,2) ------表示月薪，工资
7 ------COMM ---------NUMBER(7,2)------ 表示奖金或佣金
8------ DEPTNO-------- NUMBER(2)-------- 表示部门编号

```

```
DEPT（部门表）部门表：表示一个部门的具体信息
NO ------字段---------------- 类型 --------------描 述
1------ DEPTNO -------NUMBER(2)--------- 部门编号
2 -------DNAME-------- VARCHAR2(14)---- 部门名称
3--------- LOC ----------VARCHAR2(13)------ 部门位置
```

```
BONUS（奖金表）奖金表：表示一个雇员的工资及奖金。 NO -------字段 -----------------类型 -----------------描述
1 --------ENAME-------- VARCHAR2(10) ------雇员姓名
2--------- JOB----------- VARCHAR2(9)-------- 雇员工作
3-------- SAL----------- NUMBER-------------- 雇员 工资
4------ COMM--------- NUMBER------------- 雇员奖金

```

```
SALGRADE（工资等级表）一个公司是有等级制度，用此表表示一个工资的等级
NO--------- 字段 ---------类型 -------------------描述
1 ----------GRAD--------- NUMBER--------- 等级名 称
2--------- LOSAL------- NUMBER- -------此等级的最低工资

```

## 测试题

**–SQL练习训练一**

1、 选择部门30中的雇员 

```mysql
select * from emp where deptno=30;
```

2、 检索emp表中的员工姓名、月收入及部门编号 

```mysql
select ename,sal,deptno from emp;
```

3、 检索emp表中员工姓名、及雇佣时间（雇佣时间按照yyyy-mm-dd显示） 

```mysql
select ename,hiredate from emp;

select ename 员工姓名,date_format(hiredate,'%Y年%m月%d日') 雇佣日期 from emp;
```

4、 检索emp表中的部门编号及工种，并去掉重复行 

```mysql
select distinct deptno,job from emp;
```

5、 检索emp表中的员工姓名及全年的月收入 

```mysql
select ename,sal*12 from emp;
```

6、 用姓名显示员工姓名，用年收入显示全年月收入。 

```mysql
select ename 员工姓名,concat('￥',round(sal*12)) 年收入 from emp; 
```

7、 检索月收入大于2000的员工姓名及月收入 

```mysql
select ename,sal from emp where sal>2000;
```

8、 检索月收入在1000元到2000元的员工姓名、月收入及雇佣时间 

```mysql
select ename,sal,hiredate from emp where sal between 1000 and 2000;

改写后使用 datadiff函数进行时间的计算
select ename,sal,hiredate,datediff(DATE_FORMAT(NOW(),'%Y-%m-%d'),DATE_FORMAT(hiredate,'%Y-%m-%d')) 时间 from emp where sal between 1000 and 2000;
```

9、 检索以S开头的员工姓名及月收入 

```mysql
select ename,sal from emp where ename like ‘S%’;
```

10、检索emp表中月收入是800的或是1250的员工姓名及部门编号

```mysql
select ename,deptno from emp where sal = 800 or sal = 1250;

select ename,deptno,sal from emp where sal in (800,1250);
```

11、显示在部门20中岗位是CLERK的所有雇员信息

```mysql
select * from emp where deptno = '10' and ename = 'CLARK';
```

12、显示工资高于2500或岗位为MANAGER的所有雇员信息

```mysql
select * from emp where sal > 2500 or job = 'MANAGER';
```

13、检索emp表中有奖金的员工姓名、月收入及奖金

```mysql
select ename,sal,comm from emp where comm is not null
```

14、检索emp表中部门编号是30的员工姓名、月收入及提成，并要求其结果按月收入升序、然后按提成 降序显示

```mysql
select ename,sal,comm from emp where deptno =  30 order by sal asc,comm desc;
```

15、列出所有办事员的姓名、编号和部门



16、找出佣金高于薪金的雇员
17、找出部门10中所有经理和部门20中的所有办事员的详细资料
18、找出部门10中所有经理、部门20中所有办事员，既不是经理又不是办事员但其薪金>=2000的所有 雇员的详细资料
19、找出收取奖金的雇员的不同工作
20、找出不收取奖金或收取的奖金低于100的雇员
21、找出各月倒数第三天受雇的所有雇员
22、获取当前日期所在月的最后一天
23、找出早于25年之前受雇的雇员
24、显示正好为6个字符的雇员姓名
25、显示不带有’R’的雇员姓名
26、显示雇员的详细资料，按姓名排序
27、显示雇员姓名，根据其服务年限，将最老的雇员排在最前面
28、显示所有雇员的姓名、工作和薪金，按工作的降序顺序排序，而工作相同时按薪金升序
29、显示所有雇员的姓名和加入公司的年份和月份，按雇员受雇日所在月排序，将最早年份的项目排在 最前面

30、显示在一个月为30天的情况下所有雇员的日薪金
31、找出在（任何年份的）2月受聘的所有雇员
32、对于每个雇员，显示其加入公司的天数
33、显示姓名字段的任何位置，包含 “A” 的所有雇员的姓名
34、以年、月和日显示所有雇员的服务年限
35、选择公司中有奖金 (COMM不为空,且不为0) 的员工姓名，工资和奖金比例,按工资逆排序,奖金比 例逆排序.
36、选择公司中没有管理者的员工姓名及job
37、选择在1987年雇用的员工的姓名和雇用时间
38、选择在20或10号部门工作的员工姓名和部门号
39、选择雇用时间在1981-02-01到1981-05-01之间的员工姓名，职位(job)和雇用时间,按从早到晚 排序.
40、选择工资不在5000到12000的员工的姓名和工资
41、查询员工号为7934的员工的姓名和部门号
42、查询工资大于1200的员工姓名和工资
