# JavaScript入门

# 一、JavaScript简介

JavaScript，就是我们通常所说的JS，是一种嵌入到HTML页面中的脚本语言，由浏览器一边解释一边执行。

我们在“HTML入门教程”中的“前端技术简介”中深入浅出地讲解了HTML、CSS和JavaScript之间的关系，这一篇文章分量很重，大家没看过的记得回去看一下。

HTML、CSS和JavaScript的关系如下：

“HTML是网页的结构，CSS是网页的外观，而JavaScript是页面的行为。”

我们都知道单纯的HTML页面是静态的（只供浏览），而JavaScript的出现，把静态的页面转换成支持用户交互并响应相应事件的动态页面。那么在我们平常的浏览的网页中，都有哪些地方用到了JavaScript呢？

我们就拿绿叶学习网来说，导航、tabs选项卡、回顶部这些地方都用到了JavaScript。HTML只是一门描述性的语言，这些地方单纯地使用HTML是无法实现的，而必须使用编程的方式来实现，那就必须使用JavaScript了。


![JavaScript图片切换](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzEtMS0xLnBuZw?x-oss-process=image/format,png)

![JavaScript tabs选项卡](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzEtMS0yLnBuZw?x-oss-process=image/format,png)

![JavaScript 回顶部特效](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzEtMS0zLnBuZw?x-oss-process=image/format,png)

# 二、JavaScript的特点

HTML页面是静态的，而JavaScript可以弥补HTML语言的缺陷，实现Web页面客户端的动态效果。JavaScript的作用有以下几点：

### 1、动态改变页面内容

HTML页面是静态的，一旦编写，内容是无法改变的。JavaScript可以弥补这个不足，可以将内容动态地显示在网页中。

### 2、动态改变网页的外观

JavaScript通过修改网页元素的CSS样式，达到动态地改变网页的外观。

### 3、验证表单数据

我们常见的在各大网站中的注册中的验证功能，就是JavaScript实现的。

### 4、响应事件

JavaScript是基于事件的语言。例如点击一个按钮弹出一个对话框，就是鼠标点击触发的事件，例如绿叶学习网教程文章中的点赞效果：

对于JavaScript的理解，就一句话：如果没有使用JavaScript，网页就是静态的，唯一的功能就是给用户浏览。加入了JavaScript，网页变得绚丽多彩起来。


# 三、内容

## 1、JavaScript简介

### 一、JavaScript是什么？

1、HTML是网页的结构，CSS是网页的外观，而JavaScript是页面的行为。

2、HTML页面是静态的（只供浏览），平常我们所见到的各种网页特效就是使用JavaScript实现的。

### 二、JavaScript编辑工具

常用的JavaScript编辑工具有：

（1）记事本；

（2）Dreamweaver；

（3）UltraEdit-32；

（4）Visual Studio；

（5）sublime Text

绿叶学习网系列教程推荐使用Visual Studio作为网站开发工具。

### 三、JavaScript在HTML的引用方式

JavaScript在HTML的引用方式共有4种：

（1）页头引入（head标签内）；

（2）页中引入（body标签内）；

（3）元素事件中引入（标签属性中引入）；

（4）引入外部JS文件；

### 四、JavaScript和Java的关系

JavaScript和Java只有一毛钱的关系，无他。

JavaScript和Java虽然名字相似，但是本质上是不同的。

（1）JavaScript往往都是在网页中使用，而Java却可以在软件、网页、手机App等各个领域中使用；

（2）Java是一门面向对象的语言，而从本质上讲，JavaScript更像是一门函数式编程语言；


### 五、训练题

在进入JavaScript基础学习之前，先让大家接触一下神奇的javascript程序究竟是怎样的？

这个例子实现的功能是：在页面打开时候，弹出对话框显示“欢迎您来到绿叶学习网！”。

程序代码实现如下：

```html
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <title>第一个javascript程序</title>

    <script type="text/javascript">

        function enterMes()

        {

            alert("欢迎您来到绿叶学习网！");

        }

        window.onload=enterMes();

    </script>

</head>

<body>

</body>

</html>
```

刚刚打开这个页面时，在浏览器预览效果如下：

![javascript程序](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzEtNi0xLnBuZw?x-oss-process=image/format,png)

## 2、JavaScript入门基础

### 一、数据结构

JavaScript的数据结构包括：标识符、关键字、常量、变量等。

**1、标识符**

标识符，说白了，就是一个名字。在JavaScript中，变量和函数等都需要定义一个名字，这个名字就可以称为“标识符”。

JavaScript语言中标识符最重要的3点就是：

（1）第一个字符必须是字母、下划线（_）或美元符号这3种其中之一，其后的字符可以是字母、数字或下划线、美元符号；

（2）变量名不能包含空格、加号、减号等符号；

（3）标识符不能和JavaScript中用于其他目的的关键字同名；

**2、关键字**

JavaScript关键字是指在JavaScript语言中有特定含义，成为JavaScript语法中一部分的那些字。

**3、常量**

常量，顾名思义就是指不能改变的量。常量的指从定义开始就是固定的，一直到程序结束。

常量主要用于为程序提供固定和精确的值，包括数值和字符串，如数字、逻辑值真（true）、逻辑值假（false）等都是常量。

**4、变量**

变量，顾名思义，就是指在程序运行过程中，其值是可以改变的。

### 二、JavaScript数据类型

JavaScript数据类型有2大分类：一是“基本数据类型”，二是“特殊数据类型”。

其中，基本数据类型包括以下3种：

（1）数字型（Number型）：如整型84，浮点型3.14；
（2）字符串型（String型）：如"绿叶学习网"；
（3）布尔型（Boolean型）：true或fasle；

特殊数据类型有3种：

- （1）空值（null型）；
- （2）未定义值（undefined型）；
- （3）转义字符；

```
根据个人的开发经验中，只需要记忆\n、\'、\"这3个就已经够初学者走很远了，其他的转义字符我们完全没必要去记忆，到时候需要的时候再回来查表就行了，大家别浪费脑细胞喔。
```

### 三、运算符

JavaScript的运算符按运算符类型可以分为以下5种：

（1）算术运算符；

（2）比较运算符；

（3）赋值运算符；

（4）逻辑运算符；

（5）条件运算符；

### 四、typeof运算符

typeof运算符用于返回它的操作数当前所容纳的数据的类型，这对于判断一个变量是否已被定义特别有用。

例如：typeof(1)返回值是number，typeof("javascript")返回值是string。

### 五、表达式

表达式是一个语句的集合，计算结果是个单一值。

在JavaScript中，常见的表达式有4种：

（1）赋值表达式；

（2）算术表达式；

（3）布尔表达式；

（4）字符串表达式；

### 六、类型转换

**1、字符串型转换为数值型**

在JavaScript中，将字符串型数据转换为数值型数据有parseInt()和parseFloat()这2种方法。其中，parseInt()可以将字符串转换为整型数据；parseFloat()可以将字符串转换为浮点型数据。

语法：

```
parseInt()  //将字符串型转换为整型

parseFloat()  //将字符串型转换为浮点型
```

**2、数值型转换为字符串型**

在JavaScript中，将数值型数据（整型或浮点型）转换为字符串，都是使用toString()方法。

语法：

```
.toString()
```

### 七、JavaScript基本语法

**1、执行顺序**

JavaScript程序按照在HTML文档中出现的顺序逐行执行。如果需要在整个HTML文件中执行，最好将其放在HTML文件的标签中。某些代码，如函数体内的代码，不会被立即执行，只有当所在的函数被其他程序调用时，该代码才会被执行。

**2、区分大小写**

JavaScript是严格区分大小写的。例如str和Str这是两个完全不同的变量。

**3、分号和空格**

在JavaScript中，语句的分号“;”是可有可无的。但是我们强烈要求大家在每一句语句后面加一个分号“;”，这是一个非常重要的代码编写习惯。

另外，JavaScript会忽略多余的空格，用户可以向脚本添加空格，来提高代码的可读性，说白了就是让代码“漂亮点”，读得舒服一点。

例如：


```
var str="绿叶学习网JavaScript教程";

var str = "绿叶学习网JavaScript教程";  //这一行代码读起来舒服一点
```

### 八、JavaScript注释

在编写JavaScript代码时，我们经常要在一些关键代码旁做一下注释，这样做的好处很多。

语法：

```
//单行注释内容

/*多行注释内容*/
```

“//”是单行注释方式，如果你的注释内容只占一行就应该使用这种注释方式。“/**/”是多行注释方式，如果你的注释内容占多行建议使用这种注释方式。

## 3、流程控制

JavaScript对程序流程的控制跟其他编程语言是一样的，主要有3种：

（1）顺序结构；

（2）选择结构；

（3）循环结构；

### 一、选择结构

在JavaScript中，选择结构共有5种：

（1）if语句;

（2）if……else语句;

（3）if……else if……语句;

（4）if语句的嵌套;

（5）switch语句;

### 二、循环结构

在JavaScript中，循环结构总有3种：

（1）while语句；

（2）do……while语句；

（3）for语句；

### 三、跳转语句

JavaScript支持的跳转语句主要有2种：

（1）break语句；

（2）continue语句；

break语句与continue语句的主要区别是：break是彻底结束循环，而continue是结束本次循环。在这一点跟其他编程语言（如C和Java）相同。

### 四、训练题

（1）题目：计算100以内所有奇数的和比较简单，使用for语句就可以实现。

代码实现如下：

```html
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <title></title>

    <script type="text/javascript">

        var sum=0;

        for(var i=1;i<100;i+=2)

        {

            sum+=i;

        }

        document.write("100以内所有奇数和为："+sum);

    </script>

</head>

<body>

</body>

</html>
```

在浏览器预览效果如下：

![JavaScript计算100以内所有奇数的和](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzMtMTAtMS5wbmc?x-oss-process=image/format,png)

分析：

“i+=2”等价于“i=i+2”，“sum+=i”等价于“sum=sum+i”。这些基础的东西可别忘了。

（2）

题目：输出所有的“水仙花数”。所谓“水仙花数”是指一个3位数，其各位数字立方和等于该数的本身。例如，153就是一个水仙花数，因为153=13+53+33。

代码实现如下：

```html
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <title></title>

    <script type="text/javascript">

        document.write("水仙花数有：");

        for(var i=100;i<1000;i++)

        {

            var a=i%10;//提取个位数

            var b=(i/10)%10 //提取十位数

            b=parseInt(b);

            var c=i/100;//提取百位数

            c=parseInt(c);

            if(i==(a*a*a+b*b*b+c*c*c))

            {

                document.write(i+",");

            }

        }

    </script>

</head>

<body>

</body>

</html>
```

在浏览器预览效果如下：

![水仙花数](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzMtMTEtMS5wbmc?x-oss-process=image/format,png)

分析：

parseInt()函数是将一个数转换为整型数据，我们在“JavaScript类型转换”这一节中已经详细给大家讲解了。

（3）

题目：找出字符串中“how are you doing？”中所有小于字母“s”的字符，并在页面输出。

代码实现如下：

```html
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <title></title>

    <script type="text/javascript">

        var str="how are you doing？";

        var result="";

        for(var i=0;i<str.length;i++)

        {

            if(str.charAt(i)<"s")

            {

                result+=str.charAt(i)+",";

            }

        }

        document.write(result);

    </script>

</head>

<body>

</body>

</html>
```



在浏览器预览效果如下：

![img](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzMtMTItMS5wbmc?x-oss-process=image/format,png)

分析：

这些是字符串的内容，如果大家不懂没关系，现在只是让大家接触一下。因为例子要是太过于简单，大家学得也没有激情。这些在“JavaScript字符串对象”这一章中，我们会详细讲解到。

## 4、函数

### 一、函数是什么？

函数，就是一个一系列JavaScript语句的集合，这是为了完成某一个会重复使用的特定功能。在需要该功能的时候，直接调用函数即可，而不必每次都编写一大堆重复的代码。并且在需要修改该功能的时候，也只要修改和维护这一个函数即可。

总之，将语句集合成函数，好处就是方便代码重用。并且，一个好的函数名，可以让人一眼就知道这个函数实现的是什么功能，方便维护。

函数的使用只需要2步：

（1）定义函数；

（2）调用函数；


### 二、函数的定义

在JavaScript中，使用函数前，必须用function关键字来定义函数。

函数常用方式有2种：

（1）不指定函数名的函数；

（2）指定函数名的函数；

**1、不指定函数名的函数**

语法：

```javascript
function(参数1,参数2,….,参数n)

{

    //函数体语句

}
```

说明：

定义函数必须使用function关键字。

**2、指定函数名的函数**

“指定函数名的函数”是JavaScript中使用最广泛的方法，反而“不指定函数名的函数”用得比较少。

语法：

```javascript
function 函数名(参数1,参数2,….,参数n)

{

    //函数体语句

    return 表达式;

}
```

说明：

定义函数必须使用function关键字来定义。

函数名必须是唯一的，尽量通俗易懂，并且跟你定义的代码有关。

函数可以使用return语句将某个值返回，也可以没有返回值。

参数是可选的，可以不带参数，也可以带多个参数。如果是多个参数的话，参数之间要用英文逗号隔开。

### 三、函数的调用

常用的函数调用方式有4种：

（1）简单调用；

（2）在表达式中调用；

（3）在事件响应中调用；

（4）通过链接调用；

### 四、特殊函数

JavaScript特殊函数有3种：

- （1）嵌套函数；
- （2）递归函数；
- （3）内置函数；

下面详细给大家讲解一下这3中函数调用方式。

**1、嵌套函数**

嵌套函数，顾名思义，就是在一个函数的内部定义另外一个函数。不过在内部定义的函数只能在内部调用，如果在外部调用，就会出错。

举例：

```html
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <title></title>

    <script type="text/javascript">

        //定义阶乘函数

         function fun(a)

         {

             //嵌套函数定义，计算平方值的函数

             function multi (x)

             {

                 return x*x;

             }

             var m=1;

             for(var i=1;i<=multi(a);i++)

             {

                 m=m*i;

             }

             return m;

         }

         var sum =fun(2)+fun(3);

         document.write(sum);

    </script>

</head>

<body>

</body>

</html>
```

在浏览器预览效果如下：

![JavaScript嵌套函数](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzQtNC0xLnBuZw?x-oss-process=image/format,png)

分析：

上面定义的multi函数只能在fun函数内部使用，如果在fun函数外部调用就会出错，

**2、递归函数**

递归函数是一种非常重要的编程技术，当年我在学习其他编程技术（如C、C++、Java等）都经常用到。

递归函数用于让一个函数从其内部调用其本身。不过需要注意的是，如果递归函数处理不当，就会使程序陷入“死循环”。为了防止“死循环”的出现，可以设计一个做自加运算的变量，用于记录函数自身调用的次数，如果次数太多就让它自动退出循环。

语法：


```
function 递归函数名(参数1)

{

    递归函数名(参数2)

}
```

说明：

在定义递归函数时，需要2个必要条件：

（1）首先包括一个结束递归的条件；

（2）其次包括一个递归调用的语句；

举例：

```html
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <title></title>

    <script type="text/javascript">

         var msg="\n函数的递归调用：\n\n";

         //响应按钮的点击事件

         function Test()

         {

             var result;

             msg+="调用语句：\n";

             msg+="    result=sum(20);\n";

             msg+="调用步骤：\n";

             result=sum(20);

             msg+="计算结果：\n";

             msg+="    result="+result+"\n";

             alert(msg);

         }

         //计算当前步骤加和值

         function sum(m)

         {

             if(m==0)

             {

                 return 0;

             }

             else

             {

                 msg+="    result="+m+"+sum("+(m-2)+ ");\n";

                 result=m+sum(m-2);

             }

             return result;

         }

    </script>

</head>

<body>

    <input type="button" value="测试" onclick="Test()"/>

</body>

</html>
```

在浏览器预览效果如下：

![JavaScript递归函数](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzQtNC0yLnBuZw?x-oss-process=image/format,png)

分析：

在上述代码中，为了求取20以内的偶数和，定义了递归函数sum(m)，而函数Test()对其进行调用，并使用alert()方法弹出相应的提示信息。

递归函数对于初学者来说可能比较难以理解，如果实在不会，可以直接忽略它。因为在JavaScript中是比较少用到递归函数的，递归函数往往都是在其他编程语言中用得比较多。到时候需要的时候我们回来翻翻就OK了。


**3、内置函数**

JavaScript中有2种函数：一种是用户自定义函数，另外一种是JavaScript语言内部已经定义好了，可以直接供我们调用的函数（也就是内置函数）。

内置函数由于已经在JavaScript语言内部定义好了的，也就是我们不需要自己定义就能用了。这样极大方便了我们的编程。


# 五、JavaScript函数中的参数（arguments）

**arguments**

　argument是JavaScript中的一个关键字，用于指向调用者传入的所有参数。

```javascript
function example(x){
    alert(x); //1
    alert(arguments.length); //3
    for(var i=0; i<arguments.length; i++){
        alert(arguments[i]);  //1,2,3   
    }      
}
example(1,2,3);
```

　即使不定义参数，也可以取到调用者的参数。

```javascript
function abs() {
    if (arguments.length === 0) {
        return 0;
    }
    var x = arguments[0];
    return x >= 0 ? x : -x;
}
 
abs(); // 0
abs(10); // 10
abs(-9); // 9
```

**REST**

　由于JavaScript函数允许接收任意个参数，所以不得不用arguments来获取函数定义a以外的参数。

```javascript
function exm(a) {
    var rest = [];
    if (arguments.length > 1) {
        for (var i = 1; i<arguments.length; i++) {
            rest.push(arguments[i]);
        }
    }
}
```

　其实ES6给了新的rest参数，用在函数最后，多余的参数以数组的形式交给变量rest，如果传入的参数未填满函数定义的参数，rest会是一个空数组。

```javascript
function exm(a, b, ...rest) {
    console.log('a = ' + a);
    console.log('b = ' + b);
    console.log(rest);
}
 
exm(1, 2, 3, 4, 5);
// 结果:
// a = 1
// b = 2
// Array [ 3, 4, 5 ]
 
exm(1);
// 结果:
// a = 1
// b = undefined
// Array []
```

```javascript
<script>
		function say() {
	
			console.log('Hello' + arguments[0] + arguments[1]);
	
			console.log(arguments.length);
	
		}
	
		say('World！', 'ByeBye！');
	</script>
```

### 六、练习题

（1）

题目：在页面中添加两个单行文本框，在两个文本框中输入两个数字，使用JavaScript函数比较这两个函数大小，并使用对话框输出最大数。

实现代码如下：

举例：

```html
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <title></title>

    <script type="text/javascript">

        function maxNum()

        {

            //获取两个文本框的值

            var x = document.getElementById("num1").value;

            var y = document.getElementById("num2").value;

            //强制转换为数值型

            x = parseFloat(x);

            y = parseFloat(y);

            if(x<y)

            {

                alert("最大数是："+y);

            }

            else

            {

                alert("最大数是："+x);

            }

        }

    </script>

</head>

<body>

    第一个数是：<input type="text" id="num1"/><br/>

    第二个数是：<input type="text" id="num2"/><br/>

    <input type="button" onclick="maxNum()" value="计算"/>

</body>

</html>
```

自己的例子:

```html
<html>
    <head>
        <title></title>
        <script type="text/javascript">
            function compare(){
                var one = document.getElementById("one").value;
                var two = document.getElementById("two").value;
                one = parseFloat(one);
                two = parseFloat(two);
                console.log("one"+one);
                console.log("two"+two);
                if(one>two){
                    return alert(one) ;
                }else if(one == two){
                    alert("相等");
                }else{
                    alert(two);
                }
            }
        </script>
    </head>
    <body>
        <h3>请在两个文本框中输入两个数字可进行比较</h3><br/>
        <p>第一个数字:</p>
        <input type="text" value="" name="one" id="one"/>
        <p>第二个数字:</p>
        <input type="text" value="" name="two" id="two"/><br/>
        <input type="button" value="比较" onclick="compare()"/>
    </body>
</html>
```

在浏览器预览效果如下：

![JavaScript比较两个数大小并输出最大数](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzQtNi0xLnBuZw?x-oss-process=image/format,png)

我们随便输入两个数字如“50和100”，然后点击“计算”按钮即可输出最大数，效果如下：

![img](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzQtNi0yLnBuZw?x-oss-process=image/format,png)

分析：

这一个程序非常简单，但是包含的信息量不少。

document.getElementById()类似于CSS中的id选择器，而document.getElementById("num1").value表示选取id为num1的元素并获取它的值。这个方法经常用到，大家要记一下。我们在后续课程会给大家详细讲解。

这里用到了函数调用的其中一个方式“在事件中调用函数”。<input type="button" οnclick="maxNum()"/>表示在按钮点击的时候执行函数maxNum()。

此外，还有一点要注意的是：有些同学呀，在定义这个函数的时候定义的函数名是max，然后发现出错了！oh~，其实那是你忽略了很基础的一点，那就是自己定义的函数名是不能与JavaScript内部定义的函数名相同。


（2）

题目：写一个函数，输入一个4位数字，要求使用对话框输出这4个数字字符，但每两个数字之间留一个空格，如输入1992，应输出“1 9 9 2”。

代码实现如下：

举例：

```html
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <title></title>

    <script type="text/javascript">

        function strNum()

        {

            //获取输入文本框中的值，这里获取的值是一个字符串

            var s = document.getElementById("num").value;

            var s1="";

            for(var i=0;i<4;i++)

            {

                //如果获取的不是最后一个字符

                if(i!=3)

                {

                    s1+=s.charAt(i)+ " ";

                }

                //如果获取的是最后一个字符

                else

                {

                    s1+=s.charAt(i);

                }

            }

            alert(s1);

        }

    </script>

</head>

<body>

    输入数字是：<input type="text" id="num"/><br/>

    <input type="button" onclick="strNum()" value="提交"/>

</body>

</html>
```

在浏览器预览效果如下：

![img](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzQtNy0xLnBuZw?x-oss-process=image/format,png)

在文本框输入2015，然后点击“提交”按钮，效果如下：

![img](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzQtNy0yLnBuZw?x-oss-process=image/format,png)

分析：

这里再一次用到了document.getElementById()，而字符串操作的charAt()方法我们在上一章的练习中也接触了，详细内容我们在字符串对象这一章会讲解。程序还是比较简单的，大家琢磨一下。


## 5、内置函数

在JavaScript中，常用的内置函数有7个：

（1）eval()函数

（2）isFinite()函数

（3）isNaN()函数

（4）parseInt()函数

（5）parseFloat()函数

（6）escape()函数

（7）unescape()函数

### 一、eval()函数

在JavaScript中，eval()函数可以把一个字符串当做一个JavaScript表达式一样去执行它。例如：

```
eval("document.write('<strong>JavaScript入门教程</strong> ')");
```

上面语句说白了就是执行“document.write('**JavaScript入门教程** ')”,eval()函数用了等于没用一样。这是这种“多此一举”的做法，在实际开发很少用到eval()函数。

### 二、isFinite()函数

在JavaScript中，isFinite()函数用来确定某一个数是否是一个有限数值。

语法：

```
isFinite(number)
```

说明：

number参数是必选的，可以是任意的数值，例如整型、浮点型数据。

如果该参数为非数字、正无穷数和负无穷数，则返回false；否则的话，返回true。如果是字符串类型的数字，就会自动转化为数字型。

### 三、isNaN()函数

语法：

```
	
isNaN(参数)
```

说明：

这里的参数可以是任何类型的数据，例如数字型、字符串型、日期时间型等。不过得注意一点，当参数是“字符串类型的数字”，就会自动转换为数字型。

例如：

```
123 //这不是NaN值

"123"  //这也不是NaN值，因为“字符串类型的数字”会被自动转换为数字型

"abc123"  //这是NaN值
```

### 四、parseInt()函数和parseFloat()函数

在JavaScript中，将字符串型数据转换为数值型数据有parseInt()和parseFloat()这2种方法。其中，parseInt()可以将字符串转换为整型数据；parseFloat()可以将字符串转换为浮点型数据。

语法：

```
parseInt()  //将字符串型转换为整型

parseFloat()  //将字符串型转换为浮点型
```

说明：

将字符串型转换为整型，前提是字符串一定要是数值字符串。那什么叫数值字符串呢？“123”、“3.1415”这些只有数字的字符串就是数值字符串，而“hao123”、“360cn”等就不是数值字符串。

### 五、escape函数和unescape函数

escape()函数主要作用就是对字符串进行编码，以便它们能在所有计算机上可读。

unescape()函数和escape()函数是刚好反过来的，escape()函数是编码，unescape()函数是解码。

**1、escape函数**

escape()函数主要作用就是对字符串进行编码，以便它们能在所有计算机上可读。

语法：

```
escape(charString)
```

说明：

charString是必选参数，表示要进行编码的字符串或文字。escape()函数返回一个包含charString内容的字符串值（Unicode格式）。除了个别如“*@”之类的符号外，其余所有空格、标点符号以及其他非ASCII字符均可用“%xx”这种形式的编码代替，其中xx等于表示该字符的十六进制数。


举例：

```html
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <title></title>

    <script type="text/javascript">

        document.write(escape("hello lvye!"))

    </script>

</head>

<body>

</body>

</html>
```

在浏览器预览效果如下：

![JavaScript escape函数](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzUtNi0xLnBuZw?x-oss-process=image/format,png)

分析：

空格符对应的编码是“%20”，感叹号对应的编码是“%21”，因此执行escape("hello lvye!")后结果为“hello%20lvye%21”。

**2、unescape()函数**

escape()函数和unescape()函数是刚好反过来的，前者是编码，后者是解码。

语法：

```
unescape(charString)
```

说明：

charString是必选参数，表示要进行解码的字符串。unescape()函数返回指定值的ASCII字符串。与escape()函数相反，unescape()函数返回一个包含charString内容的字符串值，所有以“%xx”十六进制形式编码的字符都用ASCII字符集中等价的字符代替。

举例：


```html
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <title></title>

    <script type="text/javascript">

        document.write(unescape("hello%20lvye%21"))

    </script>

</head>

<body>

</body>

</html>
```

在浏览器预览效果如下：

![JavaScript unescape函数](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzUtNi0yLnBuZw?x-oss-process=image/format,png)

分析：

空格符对应的编码是“%20”，感叹号对应的编码是“%21”，因此执行unescape("hello%20lvye%21")后结果为“hello lvye!”。

### 六、训练题

**在线escape加解密工具制作**

代码实现如下：

```html
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<title></title>

    //定义基本样式

    <style type="text/css">

        #txt-input,#txt-output

        {

            height:60px;

        }

    </style>

    <script type="text/javascript">

        //定义加密函数

        function encrypt()

        {

            //获取输入框的值

            var str = document.getElementById("txt-input").value;

            //将输入框的值加密，并赋给输出框

            document.getElementById("txt-ouput").value = escape(str);

        }

        //定义解密函数

        function decrypt()

        {

            //获取输入框的值

            var str = document.getElementById("txt-input").value;

            //将输入框的值加密，并赋给输出框

            document.getElementById("txt-ouput").value = unescape(str);

        }

    </script>

</head>

<body>

    <form>

        <textarea id="txt-input" cols="20"></textarea><br />

        <input id="btn-encrypt" type="button" value="加密" onclick="encrypt()"/>

        <input id="btn-decrypt" type="button" value="解密" onclick="decrypt()"/>

        <input id="Reset1" type="reset" value="清空" /><br />

        <textarea id="txt-ouput" cols="20"></textarea>

    </form>

</body>

</html>
```

在浏览器预览效果如下（IE浏览器）：

![img](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzUtOC0yLnBuZw?x-oss-process=image/format,png)

分析：

（1）重置按钮的作用域是同一个form标签内部的文本框，这一点请参考“HTML入门教程”中的“按钮button”这一节；

（2）这里再次用到了document.getElementById()这一JavaScript的id选择器，这次大家打死都要记住这一个语句了，在之前都讲解了3次以上了；

（3）这个在线工具逻辑已经实现，界面还比较粗糙，剩下的就交给大家润色了。


## 6、字符串对象

### 一、JavaScript字符串对象简介

字符串，是程序设计中经常使用的一种数据类型，在每一种编程语言中都非常非常的重要。

这一章我们给大家详细地介绍JavaScript中的字符串对象string，然后给大家讲解一下各种操作字符串的技巧。有可能这些技巧一时半会你用不上，但是学习知识有一种说法是：你只有接触了某个知识点，即使将来你已经忘记了这个知识点具体是怎样的了，不过你却能想到用这么一个知识去帮你解决某些问题。但是，如果你没有接触这个知识点，你大脑是完全对这个知识没有概念，你连翻书的份都没，还谈解决什么问题？额，说得有点拗口，不过也是我作为程序猿多年的经验。

字符串对象string有很多方法，例如match()方法、search()方法、replace()方法等。有可能这些方法你学了一阵子都忘记具体语法是怎样的了，其实你不要埋怨自己笨，因为不经常用的知识，我们往往都容易忘记。等你需要用到这些语法了，再回来翻翻就可以了。然后经历过多次使用了，这些语法就根深蒂固了。

不过呢，在此说一下，在这一章包括之前或之后的章节，对象都有很多方法或属性，不要求每一种都掌握，但是至少我们要去看看，这样我们在以后开发中需要用到的时候，大脑“仅存”的记忆都能提醒我们要用到哪些知识点，然后我们回来翻翻就行了。


### 二、length属性简介

在JavaScript中，对于字符串来说，要掌握的属性就只有一个，那就是length属性。我们可以通过length属性来获取字符串的长度。

语法：

```
字符串名.length
```

说明：

length属性很简单，但是在字符串操作中经常要用到，这个大家一定要记住。

举例：

```html
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <title></title>

    <script type="text/javascript">

        var str="I love lvye!";

        document.write("字符串长度是："+str.length);

    </script>

</head>

<body>

</body>

</html>
```

在浏览器预览效果如下：

![JavaScript字符串length属性](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzYtMi0xLnBuZw?x-oss-process=image/format,png)

### 三、match()方法简介

在JavaScript中，使用match()方法可以从字符串内索引指定的值，或者找到一个或多个正则表达式的匹配。

语法：

```
stringObject.match(字符串)    //匹配字符串;

stringObject.match(正则表达式)  //匹配正则表达式
```

说明：

stringObject指的是字符串对象。match()方法类似于indexOf()方法，但是它返回的是指定的值，而不是字符串的位置。

下面来看一个例子，大家就懂了。

举例：

```
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <title></title>

    <script type="text/javascript">

        var str="Hello World!";

        document.write(str.match("world")+"<br/>");

        document.write(str.match("World")+"<br/>");

        document.write(str.match("worlld")+"<br/>");

        document.write(str.match("world!"));

    </script>

</head>

<body>

</body>

</html>
```

在浏览器预览效果如下：

![JavaScript match()方法](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzYtMy0xLnBuZw?x-oss-process=image/format,png)

分析：

说白了，match()方法就是用来检索一个字符串是否存在。如果存在的话，返回要检索的字符串；如果不存在的话，返回null。

### 四、search()方法简介

在JavaScript中，search() 方法用于检索字符串中指定的子字符串，或检索与正则表达式相匹配的子字符串。

语法：

```
stringObject.search(字符串)      //检索字符串;

stringObject.search(正则表达式)  //检索正则表达式
```

说明：

stringObject指的是字符串对象。search()方法返回的是子字符串的起始位置，如果没有找到任何匹配的子串，则返回-1。

举例：

```html
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <title></title>

    <script type="text/javascript">

        var str = "I love lvyestudy!";

        document.write(str.search("lvye")+"<br/>");

        document.write(str.search("html"));

    </script>

</head>

<body>

</body>

</html>
```

在浏览器预览效果如下：

![JavaScript search()方法](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzYtNC0xLnBuZw?x-oss-process=image/format,png)

分析：

str.search("lvye")表示检索字符串"I love lvyestudy!"是否存在子字符串"lvye"，由于存在，所以返回"lvye"在字符串"I love lvyestudy!"中的起始位置7（字符串索引从0开始）。

str.search("html")表示检索字符串"I love lvyestudy!"是否存在子字符串"html"，由于不存在，因此返回-1。


### 五、indexOf()方法简介

在JavaScript中，可以使用indexOf() 方法可返回某个指定的字符串值在字符串中首次出现的位置。

语法：

```
	
stringObject.indexOf(字符串)
```

说明：

stringObject表示字符串对象。indexOf()方法跟search()方法差不多，跟match()方法类似，不同的是indexOf()方法返回的是字符串的位置，而match()方法返回的是指定的字符串。

举例：

```html
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <title></title>

    <script type="text/javascript">

        var str="Hello World!";

        document.write(str. indexOf ("world")+"<br/>");

        document.write(str. indexOf ("World")+"<br/>");

        document.write(str. indexOf ("worlld")+"<br/>");

        document.write(str. indexOf ("world!"));

    </script>

</head>

<body>

</body>

</html>

```

在浏览器预览效果如下：

![JavaScript indexOf()方法](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzYtNS0xLnBuZw?x-oss-process=image/format,png)

### 六、replace()方法简介

在JavaScript中，replace()方法常常用于在字符串中用一些字符替换另一些字符，或者替换一个与正则表达式匹配的子串。

语法：

```
stringObject.replace(原字符,替换字符)  
stringObject.replace(正则表达式,替换字符) ``//匹配正则表达式
```

举例：

```html
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <title></title>

    <script type="text/javascript">

        var str ="I love javascript!";

        var str_new=str.replace("javascript","lvyestudy");

        document.write(str_new);

    </script>

</head>

<body>

</body>

</html>
```

在浏览器预览效果如下：

![JavaScript replace()方法](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzYtNi0xLnBuZw?x-oss-process=image/format,png)

分析：

str.replace("javascript","lvyestudy")表示用"lvyestudy"替换str中的"javascript"。

### 七、charAt()方法简介

在JavaScript中，可以使用charAt()方法来获取字符串中的某一个字符。这个方法我们在之前的教程中已经多次接触了。这个方法非常好用，在实际开发中也经常用到。

语法：

```
stringObject.charAt(n)
```

说明：

string.Object表示字符串对象。n是数字，表示字符串中第几个字符。注意，字符串中第一个字符的下标是0，第二个字符的下标是1，以此类推。

举例：

```
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <title></title>

    <script type="text/javascript">

        var str = "Hello lvye!";

        document.write(str.charAt(0)+"<br/>");

        document.write(str.charAt(4));

    </script>

</head>

<body>

</body>

</html>
```

在浏览器预览效果如下：

![JavaScript charAt()方法](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzYtNy0xLnBuZw?x-oss-process=image/format,png)

### 八、字符串英文大小写转化

在JavaScript中，使用toLowerCase()和toUpperCase()这两种方法来转化字符串的大小写。其中，toLowerCase()方法将大写字符串转换为小写字符串；toUpperCase()将小写字符串转换为大写字符串。

语法：

```
字符串名. toLowerCase()  ``//将大写字符串转换为小写字符串
字符串名. toUpperCase()  ``//将小写字符串转换为大写字符串
```

说明：

此外，还有2种大小写转化方法：toLocaleLowerCase()和toLocaleUpperCase()。这两个方法我们有可能一辈子都用不到，大家要是别的书籍中看到，可以直接忽略。

举例：

```html
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <title></title>

    <script type="text/javascript">

        var str="Hello Wordl!";

        document.write("以正常方式显示为："+str+"<br/>");

        document.write("以小写方式显示为："+str. toLowerCase()+"<br/>");

        document.write("以大写方式显示为："+str. toUpperCase());

    </script>

</head>

<body>

</body>

</html>
```

在浏览器预览效果如下：

![JavaScript英文大小写转换](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzYtOC0xLnBuZw?x-oss-process=image/format,png)

### 九、连接字符串

在JavaScript中，可以使用concat()方法来连接2个或多个字符串。

语法：

```
字符串1.concat(字符串2,字符串3,…,字符串n);
```

说明：

concat()方法将“字符串2,字符串3,…,字符串n”按照顺序连接到字符串1的尾部，并返回连接后的字符串。

举例：

```php+HTML
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <title></title>

    <script type="text/javascript">

        var str1="毛扇指千阵，";

        var str2="铁马踏冰河，";

        var str3="黄沙破楼兰。";

        var str4=str1+str2+str3;

        var str5=str1.concat(str2,str3);

        document.write(str4+"<br/>");

        document.write(str5);

    </script>

</head>

<body>

</body>

</html>
```

在浏览器预览效果如下：

![JavaScript连接字符串concat()方法](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzYtOS0xLnBuZw?x-oss-process=image/format,png)

分析：

大家可以看到，原来连接字符串可以有2种方式，一种是使用concat()方法，另外一种更加简单，使用“+”运算符就可以了。在这里大家也明白我的良苦用心了吧。就是说，以后大家连接字符串别傻乎乎地用concat()方法，直接将字符串相加就可以了。

### 十、比较字符串

在javascript中，可以使用localeCompare()方法用本地特定的顺序来比较两个字符串。

语法：

```
字符串1.localeCompare(字符串2)
```

说明：

比较完成后，返回值是一个数字。

（1）如果字符串1小于字符串2，则返回小于0的数字；

（2）如果字符串1大于字符串2，则返回数字1；

（3）如果字符串1等于字符串2，则返回数字0；

举例：

```html
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <title></title>

    <script type="text/javascript">

        var str1= "JavaScript";

        var str2 = "javascript";

        var str3 = str1.localeCompare(str2);

        document.write(str3);

    </script>

</head>

<body>

</body>

</html>
```

在浏览器预览效果如下：

![JavaScript localeCompare()方法](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzYtMTAtMS5wbmc?x-oss-process=image/format,png)

### 十一、split()方法

在javascript中，可以使用split()方法把一个字符串分割成字符串数组。

语法：

```
字符串.split(分割符)
```

说明：

分割符可以是一个字符、多个字符或一个正则表达式。分割符并不作为返回数组元素的一部分。

举例：

```html
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <title></title>

    <script type="text/javascript">

        var str ="I love lvyestudy!";

        var arr=new Array();

        arr=str.split(" ");

        document.write(arr);

    </script>

</head>

<body>

</body>

</html>
```

在浏览器预览效果如下：

![JavaScript分割字符串 split()方法](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzYtMTEtMS5wbmc?x-oss-process=image/format,png)

分析：

str.split(" ")表示字符串以空格作为分割符，而arr其实就是一个数组。。split()方法虽然简单，但是在实际开发中经常会用到，大家一定要记住喔。

对于数组，我们会在数组对象那一章详细讲解。

### 十二、从字符串提取字符串

在JavaScript中，可以使用substring()方法来提取字符串中的某一部分字符串。

语法：

```
字符串.substring(开始位置,结束位置)
```

说明：

开始位置是一个非负的整数，表示从哪个位置开始截取。结束位置也是一个非负的整数，表示在哪里结束截取。

举例：

```html
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <title></title>

    <script type="text/javascript">

        var str1="绿叶学习网JavaScript教程";

        var str2=str1.substring(5,15);

        document.write(str2);

    </script>

</head>

<body>

</body>

</html>
```

# JavaScript 入门（下）

## 1、数组对象

### 一、数组基础

**1、数组是什么？**

在JavaScript中，我们可以使用“数组”来存储一组“相同数据类型”的数据结构。

**2、创建数组**

在JavaScript中，创建数组共有3种方法：

（1）新建一个长度为0的数组

举例：

```
var myArr = new Array();
```

（2）新建长度为n的数组

举例：

```
var myArr = new Array(3);

myArr[0]="HTML";

myArr[1]="CSS";

myArr[2]="JavaScript";
```

（3）新建指定长度的数组，并赋值

举例：

```
var myArr = new Array(1,2,3,4);
```

注意，在JavaScript中，数组的索引是从0开始的，而不是从1开始的。

**3、数组元素的赋值与获取**

在JavaScript中，数组元素的赋值与获取都是通过数组下标来实现。

### 二、数组的属性和方法

**1、数组的属性**

在Array对象中有3个属性，分别是length、constructor和prototype。在初学者阶段，我们仅仅掌握length这个属性就可以了。

**2、数组的方法**

​                                                                                Array对象常用方法

​       

| **方法**   | **说明**                 |
| ---------- | ------------------------ |
| slice()    | 获取数组中的某段数组元素 |
| unshift()  | 在数组开头添加元素       |
| push()     | 在数组末尾添加元素       |
| shift()    | 删除数组中第一个元素     |
| pop()      | 删除数组最后一个元素     |
| toString() | 将数组转换为字符串       |
| join()     | 将数组元素连接成字符串   |
| concat()   | 多个数组连接为字符串     |
| sort()     | 数组元素正向排序         |
| reverse()  | 数组元素反向排序         |

## 2、数值对象

下面都是这一章所讲解到的Math对象比较重要的方法：

​                                                                             Math对象的方法

| **方法** | **说明**                   |
| -------- | -------------------------- |
| max(x,y) | 返回x和y中的最大值         |
| min(x,y) | 返回x和y中的最小值         |
| pow(x,y) | 返回x的y次幂               |
| abs(x)   | 返回数的绝对值             |
| round(x) | 把数四舍五入为最接近的整数 |
| random() | 返回0~1之间的随机数        |
| ceil(x)  | 对一个数进行上舍入         |
| floor(x) | 对一个数进行下舍入         |

## 3、窗口对象

现在大家都知道了，假如我们要学习一个“XXX对象”，那肯定学习它的属性和方法。window对象也是同样的道理。

关于window对象的属性很多，但是我们在JavaScript入门阶段一个都用不着！所以这一章大家只需要认真掌握window对象的方法即可。

​                                                                             window对象方法

| **方法**                       | **说明**                       |
| ------------------------------ | ------------------------------ |
| open()、close()                | 打开窗口、关闭窗口             |
| resizeBy()、resizeTo()         | 改变窗口大小                   |
| moveBy()、moveTo()             | 移动窗口                       |
| setTimeout()、clearTimeout()   | 设置或取消“一次性”执行的定时器 |
| setInterval()、clearInterval() | 设置或取消“重复性”执行的定时器 |

### 一、窗口对象简介

之前几个章节都是JavaScript的基础部分，大家要真想掌握JavaScript，就必须得把基础给打扎实。这一章我们进入JavaScript的核心技术，其实核心技术部分也是比较简单的，大家不要一看到就退缩。

在之前我们接触过很多JavaScript对象，例如什么数组对象Array、日期对象Date等。.这一章我们来学习JavaScript最核心的对象之一：window对象。

在JavaScript中，一个浏览器窗口就是一个window对象。window对象主要用来控制由窗口弹出的对话框、打开窗口或关闭窗口、控制窗口的大小和位置等等。一句话，window对象就是用来操作“浏览器窗口”的一个对象。


**1、window对象属性**

市面上很多教程在讲解window、document等对象时，不管是常用的还是不常用的属性和方法，先一上来就帮你全部列出来，读者看得头都大。其实在JavaScript入门阶段，对于window对象的属性和方法，我们只需要掌握一些常用的就行，其他需要深入的我们在JavaScript进阶再给大家详细探讨。

关于window对象的属性很多，但是我们在JavaScript入门教程阶段一个都用不着！所以这一章大家只需要认真掌握window对象的方法即可。


**2、window对象方法**

​                                                                                window对象方法

| **方法**                       | **说明**                       |
| ------------------------------ | ------------------------------ |
| open()、close()                | 打开窗口、关闭窗口             |
| resizeBy()、resizeTo()         | 改变窗口大小                   |
| moveBy()、moveTo()             | 移动窗口                       |
| setTimeout()、clearTimeout()   | 设置或取消“一次性”执行的定时器 |
| setInterval()、clearInterval() | 设置或取消“重复性”执行的定时器 |

上面，我把window对象属性去掉，并且筛选出最常用的方法。在JavaScript入门之时，只需要掌握这些就已经足够我们走很远了。

### 二、打开和关闭窗口

在JavaScript中，打开和关闭新的窗口，这是很常见的一种操作。

在绿叶学习网的JavaScript在线测试工具中，当点击“调试代码”按钮时，就会打开一个新的窗口，并把HTML文档输出到新的页面中去。这里面涉及的方法，就是这一节我们要说到的“使用JavaScript打开和关闭窗口”。


**1、JavaScript打开窗口**

在JavaScript中，我们可以使用window对象中的open()方法来打开一个新窗口。

语法：

```
window.open(URL, 窗口名称, 参数);
```

说明：

URL：指的是打开窗口的地址，如果URL为空字符串，则浏览器打开一个空白窗口，并且可以使用document.write()方法动态输出HTML文档。

窗口名称：指的是window对象的名称，可以是a标签或form标签中target属性值。如果指定的名称是一个已经存在的窗口名称，则返回对该窗口的引用，而不会再新打开一个窗口。

参数：对打开的窗口进行属性设置。

​                                                                                参数以及说明

| **方法**   | **说明**                                 |
| ---------- | ---------------------------------------- |
| top        | 窗口顶部距离屏幕顶部的距离，默认单位为px |
| left       | 窗口左边距离屏幕左边的距离，默认单位为px |
| width      | 窗口的宽度，默认单位为px                 |
| height     | 窗口的高度，默认单位为px                 |
| scrollbars | 是否显示滚动条                           |
| resizable  | 窗口大小是否固定                         |
| toolbar    | 浏览器工具条，包括前进或后退按钮         |
| menubar    | 菜单条，一般包括文件、编辑及其它一些条目 |
| location   | 地址栏，是可以输入URL的浏览器文本区      |

这些可选参数都不是那么常用，大家不记住也没啥关系，以后需要的时候回到这里查询一下就OK了。

下面举几个常用的窗口打开的例子：

（1）打开一个新窗口

```
window.open("http://www.lvyestudy.com","","");
```

上面是打开一个新窗口，并且在新窗口加载绿叶学习网首页。

（2）打开一个指定位置的窗口：

```
window.open("http://www.lvyestudy.com ","","top=200,left=200");
```

（3）打开一个指定大小的窗口：

```
window.open("http://www.lvyestudy.com ","","width=200,height=200");
```

（4）打开一个固定大小的窗口：

```
window.open("http://www.lvyestudy.com ","","width=200,height=200,resizable");
```

（5）打开一个显示滚动条的窗口：

```
window.open("http://www.lvyestudy.com ","","width=200,height=200,scrollbars");
```

举例：

```html
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <title></title>

    <script type="text/javascript">

        function openWindow() {

            window.open("http://www.lvyestudy.com ", "", "width=200,height=200,resizable");

        }

    </script>

</head>

<body>

    <input id="btn" type="button" value="打开窗口" onclick="openWindow()"/>

</body>

</html>
```

在浏览器预览效果如下：

![JavaScript打开窗口](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzEwLTItMS5wbmc?x-oss-process=image/format,png)

分析：

![JavaScript window.open()](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzEwLTItMi5wbmc?x-oss-process=image/format,png)

此外还需要注意一点，window.open()方法中的参数width和height设置的时候是不需要加单位（px）的，浏览器默认就已经给我们添加单位。

**2、JavaScript关闭窗口**

在JavaScript中，我们可以使用window对象中的close()方法来关闭一个窗口。

（1）、关闭当前窗口

在JavaScript中，如果想要关闭当前的窗口，有3种方式：

```
window.close();

close();

this.close();
```

（2）、关闭子窗口

所谓的“关闭子窗口”就是关闭之前使用window.open()方法动态创建的子窗口。

语法：

```
窗口名.close();
```

说明：

使用window.open()方法动态创建的窗口时，我们可以将窗口以变量形式保存，然后再使用close()方法关闭动态创建的窗口。

举例：

```html
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <title></title>

    <script type="text/javascript">

        var newWindow = window.open("http://www.lvyestudy.com","","width=200,height=200");

        function closeWindow()

        {

            newWindow.close();

        }

    </script>

</head>

<body>

<input type="button" value="关闭窗口" onclick="closeWindow()"/>

</body>

</html>
```

在浏览器预览效果如下：

![JavaScript关闭窗口](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzEwLTItMy5wbmc?x-oss-process=image/format,png)

​                                                                                             父窗口

![JavaScript window.close()](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzEwLTItNC5wbmc?x-oss-process=image/format,png)

​                                                                                              子窗口

分析：

你会看到，浏览器会打开2个窗口。当我们点击“关闭窗口”按钮后，被打开的“子窗口”就会被关闭。

### 三、改变窗口大小

在JavaScript中，可以使用window对象的resizeTo()方法或resizeBy()方法来改变窗口的大小。

**1、resizeTo()方法**

语法：

```
window.resizeTo(x, y)
```

说明：

x表示改变后的水平宽度，y表示改变后的垂直高度。x和y的单位都是px，浏览器自带单位，我们只需要使用数值即可。

举例：

```
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <title></title>

    <script type="text/javascript">

        function resizeWindow()

        {

            window.resizeTo(200,200);

        }

    </script>

</head>

<body>

    <input type="button" value="改变大小" onclick="resizeWindow()"/>

</body>

</html>
```

在浏览器预览效果如下：

![JavaScript resizeTo()方法](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzEwLTMtMS5wbmc?x-oss-process=image/format,png)

分析：

当我们点击“改变大小”按钮之后，当前窗口的的宽度为200px，而高度变为200px。注意resizeTo(x,y)方法中的x和y是不需要加px作为单位的，因为浏览器默认已经带有px作为单位。

**2、resizeBy()方法**

语法：

```
	window.resizeBy(x, y)
```

说明：

当x、y的值大于0时为扩大，小于0时为缩小。其中x和y的单位都是px。

x表示窗口水平方向每次扩大或缩小的数值，y表示垂直方向窗口每次扩大或缩小的数值。

resizeTo(x,y)与resizeBy(x,y)不同在于：resizeTo(x,y)中的x、y是“改变后”的数值，而resizeBy(x,y)中的x、y是“增加或减少”的数值。“to”表示一个结果，“by”表示一个过程，大家好好琢磨“to”和“by”的英文含义就懂了。

举例：


```html
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <title></title>

    <script type="text/javascript">

        function resizeToWindow() {

            window.resizeTo(200,200);

        }

        function resizeByWindow() {

            window.resizeBy(50,50);

        }

    </script>

</head>

<body>

    <input type="button" value="resizeTo" onclick="resizeToWindow()"/>

    <input type="button" value="resizeBy" onclick="resizeByWindow()"/>

</body>

</html>
```

### 四、窗口历史

平常在使用浏览器当中，我们都会经常使用浏览器中的“前进”和“后退”。其实浏览器都会帮我们保存浏览的历史（即窗口历史）。那么在JavaScript中，我们该如何来操作这些窗口历史呢？

在JavaScript中，我们使用window对象中的history对象进行访问窗口历史。很多人对“window对象中的history对象”这一句不太理解，其实“对象里面也可以有子对象”的。看过JSON教程的读者都知道，我们可以在一个对象里面再定义一个子对象。

我们在JavaScript进阶教程的“JavaScript对象”这一节再详细为大家探讨。

既然提到对象，那肯定少不了还是按照“对象的属性”和“对象的方法”这两个套路来给大家讲解了。

1、history对象属性

在JavaScript中，hisotry对象常用的属性如下：


​                                                                        history对象属性

| **属性** | **说明**                                 |
| -------- | ---------------------------------------- |
| current  | 当前窗口的URL                            |
| next     | 历史列表中的下一个URL                    |
| previous | 历史列表中的前一个URL                    |
| length   | 历史列表的长度，用于判断列表中的入口数目 |

这些history对象属性比较少用，大家看一下就可以了。

**2、history对象方法**

在JavaScript中，hisotry对象常用的方法如下：

​                                                                        history对象方法

| **方法**  | **说明**       |
| --------- | -------------- |
| go()      | 进入指定的网页 |
| back()    | 返回上一页     |
| forward() | 进入下一页     |

我们常见的“上一页”与“下一页”实现代码如下：

语法：

```
<a href="javascript:window.history.forward();">下一页</a>

<a href="javascript:window.history.back();">上一页</a>
```

注意一下，这种“上一页”与“下一页”是针对浏览器历史记录而言，不能用来制作类似绿叶学习网那种分页特效。两者是完全不同的概念！

![JavaScript窗口历史](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzEwLTUtMS5wbmc?x-oss-process=image/format,png)

​                                                                                      分页效果

当然，我们还可以使用hisotry.go()方法指定要访问的历史记录。若参数为正数，则向前移动；若参数为负数，则向后移动，例如：

```
<a href="javascript:window.history.go(-1);">向后退1次</a>

<a href="javascript:window.history.back(2);">向后前进2次</a>
```

使用history.length属性能够访问history数组的长度，可以很容易地转移到列表的末尾，例如：

```
<a href="javascript:window.history.length-1;">末尾</a>
```

在JavaScript中，操作窗口历史语法很简单，也不是那么常用。一般情况下，在404页面中，为了用户体验，往往会有一个提供“返回上一页”的选项，这其中就用到了下面这种语法：

```
<a href="javascript:window.history.go(-1);">返回上一页</a>
```

由于窗口历史必须在实际环境才会有效，即使提供在线测试也不会有效果，请大家自行在本地测试。

### 五、定时器

什么叫定时器？我们可以看到绿叶学习网首页有一个“图片轮播”特效，每隔2s图片变换一次，这里就用到了定时器。啊，定时器，太重要了！天塌下来，大家都要扛着把它学会先。

定时器用途非常广，在图片轮播、在线时钟、弹窗广告等地方大显身手。凡是自动执行的东西，很大可能都是跟定时器有关。

在JavaScript中，关于定时器的实现，我们有2组方法：

（1）setTimeout()和clearTimeout()；
（2）setInterval()和clearInterval()；
这一节的例子使用“在线测试”不会有效果，请大家自行把代码复制到本地编辑器进行测试预览。下面我们详细分析这2组方法的用法与不同。

**1、setTimeout()和clearTimeout()**

在JavaScript中，我们可以使用setTimeout()方法来设置“一次性”调用的函数。其中clearTimeout()可以用来取消执行setTimeout()方法。

语法：

```
var 变量名 = setTimeout(code , time);
```

说明：

参数code可以是一段代码，也可以是一个调用的函数名；

参数time表示时间，表示要过多长时间才执行code中的内容，单位为毫秒。

举例：参数code是一段代码

```html
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <title></title>

    <script type="text/javascript">

        window.onload = function () {

            setTimeout("alert('欢迎来到绿叶学习网');", 2000);

        }

    </script>

</head>

<body>

    <p>2秒后提示欢迎语。</p>

</body>

</html>
```

在浏览器预览效果如下：

![setTimeout()和clearTimeout()](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzEwLTYtMS5wbmc?x-oss-process=image/format,png)

分析：

打开页面2秒后，浏览器会弹出欢迎语。由于setTimeout()方法只会执行一次，所以只会弹出一次对话框。弹出对话框如下图：

![JavaScript定时器](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzEwLTYtMi5wbmc?x-oss-process=image/format,png)

举例2：参数code是一个函数名

**2、setInterval()和clearInterval()**

在JavaScript中，我们可以使用setInterval()方法来设置“重复性”调用的函数。其中clearInterval()可以用来取消执行setTimeout()方法。

语法：

```
var 变量名 = setInterval (code , time);
```

说明：

参数code可以是一段代码，也可以是一个调用的函数名；

参数time表示时间，表示要过多长时间才执行code中的内容，单位为毫秒。

setTimeout()方法与setInterval()方法的语法很相似，实际上这两者在用法方面区别非常大。其中setTimeout()方法内的代码只会执行一次，而setInterval()方法内的代码会重复性执行，除非你使用clearInterval()方法来取消执行。

举例：


```html
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<title></title>

    <script type="text/javascript">

        //定义全局变量，用于记录秒数

        var n = 5;

        window.onload = function () {

            //设置定时器，重复执行函数countDown()

            var t = setInterval("countDown()", 1000);

        }

        //定义函数

        function countDown() {

            //判断n是否大于0，因为倒计时不可能有负数

            if (n > 0){

                n--;

                document.getElementById("num").innerHTML = n;

            }

        }

    </script>

</head>

<body>

    <p>新年倒计时：<span id="num">5</span></p>

</body>

</html>
```

分析：

window.onload表示在页面加载完毕执行，在“JavaScript页面相关事件”我们会详细讲解到。

setInterval()方法会重复执行某一段代码或函数。如果这个例子使用setTimeout方法就不能实现了，因为setTimeout()方法只会执行一次，而setInterval()会重复执行。

举例：


```html
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <title></title>

    <script type="text/javascript">

        //定义全局变量，用于记录秒数

        var n = 0;

        window.onload = function () {

            //设置定时器，重复执行函数add()

            var t = setInterval("add()", 1000);

            //点击“暂停”按钮事件

            document.getElementById("btn_pause").onclick = function () {

                clearInterval(t);

            }

            //点击“继续”按钮事件

            document.getElementById("btn_continue").onclick = function () {

                if (confirm("你还要继续装逼？")){

                    t = setInterval("add()", 1000);

                }

            }

        }

        //定义计时函数

        function add() {

            n++;

            document.getElementById("num").innerHTML = n;

        }

    </script>

</head>

<body>

    <p>你已经装了<span id="num">0</span>秒的“逼”，赶紧暂停吧！</p>

    <input id="btn_pause" value="暂停" type="button"/>

    <input id="btn_continue" value="继续" type="button" />

</body>

</html>
```

在浏览器预览效果如下：

![JavaScript setInterval()和clearInterval()](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzEwLTYtNS5wbmc?x-oss-process=image/format,png)

分析：

这里我做了一个小程序，其中使用setInterval()方法重复执行计时函数，并且利用按钮点击事件配合clearInterval()方法来进行“暂停”。

对于初学者来说，这个程序可能有点复杂，涉及了JavaScript对话框、DOM操作和JavaScript事件。请大家学习了后续课程再来看看。

这一节，我们把定时器的语法简单给大家做个介绍，在后面课程或者以后开发中，你们会经常见到定时器的身影。
总结

1、在JavaScript中，关于定时器的实现，我们有2组方法：

（1）setTimeout()和clearTimeout()；
（2）setInterval()和clearInterval()；
2、setTimeout()方法内的代码只会执行一次，而setInterval()方法内的代码会重复性执行。

## 4、JavaScript对话框

在JavaScript中，对话框共有3种，这3种对话框分别使用以下3种方法定义：

- （1）alert()；
- （2）confirm()；
- （3）prompt()；

其中前两种用得比较多，最后一种在实际开发中用得比较少。

### 一、alert()

在JavaScript中，我们可以使用window对象中的alert()方法来弹出一个提示框。该对话框效果如下：

![JavaScript alert()方法](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzExLTUtMS5wbmc?x-oss-process=image/format,png)

语法：

```
alert(message)
```

说明：

该对话框只是用于提示，并不能对JavaScript脚本产生任何影响。message是必选参数，即提示框的信息，这是一个字符串。alert()方法没有返回值。

### 二、confirm()

在JavaScript中，confirm()方法对话框一般用于确认信息，它只有一个参数，返回值为true或false。该对话框效果如下：

![JavaScript confirm()方法](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzExLTUtMi5wbmc?x-oss-process=image/format,png)

语法：

```
confirm(message)
```

说明：

message是必选项，表示弹出对话框中的文本，这是一个字符串。如果用户点击“确定”，则confirm()返回true。如果用户点击“取消”按钮，则confirm()返回false。confirm()方法往往都是和按钮结合使用。

### 三、prompt()

在JavaScript中，prompt()方法对话框用于输入并返回用户输入的字符串。该对话框效果如下：

![JavaScript prompt()方法](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzExLTUtMy5wbmc?x-oss-process=image/format,png)

语法：

```
prompt(message);
```

说明：

参数message表示对话框提示内容，这是一个字符串。

总结

1、这3种对话框特点如下：

（1）alert()：仅有提示文字，没有返回值；

（2）confirm()：具有提示文字，返回“布尔值”（true或false）；

（3）prompt()：具有提示文字，返回“字符串”；

## 5、文档对象

### 一、document对象简介

前两章，我们已经把window对象详细给大家探讨了一遍。这次我们介绍另一个网页中核心的对象：“document对象”。注意，document对象是window对象中的子对象。

谈到document对象，其实我们在之前的课程中已经接触很多次了。还记得document.write()吗？这就是document对象的一个方法。

document对象除了write()方法外，跟其他对象一样，也有自身一套属性和方法。

document对象，即文档对象。顾名思义，其实很好理解，文档对象嘛，操作的都是HTML文档。为了更好理解什么叫“HTML文档”，请看一下CSS入门教程中“正常文档流”这一节。


**1、document对象属性**

​                                                                                document对象属性

| **属性**         | 说明                      |
| ---------------- | ------------------------- |
| title            | 文档标题，即title标签内容 |
| URL              | 文档地址                  |
| fileCreateDate0  | 文档创建日期              |
| fileModifiedDate | 文档修改时间（精确到天）  |
| lastModified     | 文档修改时间（精确到秒）  |
| fileSize         | 文档大小                  |
| fgColor          | 定义文档的前景色          |
| bgColor          | 定义文档的背景色          |
| linkColor        | 定义“未访问”的超链接颜色  |
| alinkColor       | 定义“被激活”的超链接颜色  |
| vlinkColor       | 定义“访问过”的超链接颜色  |

**2、document对象方法**

​                                                                             document对象方法

| **方法**                     | **说明**                                   |
| ---------------------------- | ------------------------------------------ |
| document.write()             | 输入文本到当前打开的文档                   |
| document.writeIn()           | 输入文本到当前打开的文档，并添加换行符“\n” |
| document.getElementById()    | 获取某个id值的元素                         |
| document.getElementsByName() | 获取某个name值的元素，用于表单元素         |

上面列出了document对象常用的属性和方法，跟window对象的学习一样，在JavaScript入门阶段，站长只会给大家讲解最实用的。对于那种压根儿用不上的，我也懒得花时间去写。

### 二、训练题（1）网页动态标题

在浏览网页的时候，我们经常看到不少网页标题在闪动。这一节我们给大家讲解一个实际开发中的技巧“网页动态标题”。

实现代码如下：

```html
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <title></title>

    <script type="text/javascript">

        //定义全局变量，用于标识

        var flag = 0;

        window.onload = function () {

            //定时器

            setInterval("titleChange()", 1000);

        }

        //定义函数

        function titleChange() {

            if(flag==0)

            {

                document.title = "★☆★绿叶学习网★☆★";

                flag = 1;

            }

            else

            {

                document.title = "☆★☆绿叶学习网☆★☆";

                flag = 0;

            }

        }

    </script>

</head>

<body>

</body>

</html>
```

在浏览器预览效果如下：

![JavaScript实现网页动态标题_](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzEyLTgtMS5wbmc?x-oss-process=image/format,png)

分析：

这个例子使用在线测试不会有效果，请大家在本地编辑器中测试预览。

这里主要使用了页面加载事件window.onload和JavaScript定时器。算法很简单，只要使用了一个全局变量作为标识。

## 5、DOM对象

DOM，全称“Document Object Model（文档对象模型）”，它是由W3C组织定义的一个标准。

很多书籍一上来就大篇幅地展开说明DOM的历史和定义，看了老半天也不知道DOM是什么鬼。在这里，关于DOM的风流逸事就不展开了，免得初学者看得一头雾水，也浪费时间。

在前端开发时，我们往往需要在页面某个地方添加一个元素或者删除元素，这种添加元素、删除元素的操作就是通过DOM来实现的。

说白了，DOM就是一个接口，我们可以通过DOM来操作页面中各种元素，例如添加元素、删除元素、替换元素等。这下大家就懂了吧。

记住，DOM就是文档对象模型，文档对象模型就是DOM，很多人在学习DOM的时候看到“文档对象模型”还不知道是什么？= =||

在DOM学习中，记住最重要的一句话：每一个元素节点都看成一个“对象”。由于元素节点也是一个对象，因此他们有自身的属性和方法。


### 一、DOM节点属性

​                                                                               DOM常用的节点属性

| **属性**        | **说明**                     |
| --------------- | ---------------------------- |
| parentNode      | 获取当前节点的父节点         |
| childNodes      | 获取当前节点的子节点集合     |
| firstChild      | 获取当前节点的第一个子节点   |
| lastChild       | 获取当前节点的最后一个子节点 |
| previousSibling | 获取当前节点的前一个兄弟节点 |
| nextSibling     | 获取当前节点的后一个兄弟节点 |
| attributes      | 元素的属性列表               |

二、DOM节点操作
在JavaScript中，可以通过以下2种方式来选中指定元素：

（1）getElementById()；

在JavaScript中，如果想通过id来选中元素，我们可以使用document对象的getElementById()方法。

getElementById()方法类似于CSS中的id选择器。

语法：

```
document.getElementById("元素id");
```

举例：

```html
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <title></title>

</head>

<body>

    <div id="lvye">绿叶学习网JavaScript入门教程</div>

    <script type="text/javascript">

        var e = document.getElementById("lvye");

        e.style.color = "red";

    </script>

</body>

</html>
```

在浏览器预览效果如下：

![getElementById()](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzEzLTQtMS5wbmc?x-oss-process=image/format,png)

分析：

这里使用document.getElementById()的方法获取id为lvye的div元素，然后把这个DOM对象赋值给变量e，然后使用DOM对象的style对象来设置div元素颜色为红色。

（2）getElementsByName()；

在JavaScript中，如果想通过name来选中元素，我们可以使用document对象的getElementsByName()方法。

语法：

```
document.getElementsByName("表单元素name值");
```

说明：

getElementsByName()方法都是用于获取表单元素。

与getElementById()方法不同的是，使用该方法的返回值是一个数组，而不是一个元素。因此，我们想要获取具体的某一个表单元素，就必须通过数组下标来获取。

注意，是getElementsByName()而不是getElementByName()。数组嘛，当然是复数。

举例：

```html
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <title></title>

</head>

<body>

    <input name="web" id="radio1" type="radio" value="HTML"/>

    <input name="web" id="radio2" type="radio" value="CSS"/>

    <input name="web" id="radio3" type="radio" value="JavaScript"/>

    <input name="web" id="radio4" type="radio" value="jQuery"/>

    <script type="text/javascript">

        alert(document.getElementsByName("web")[0].value);

    </script>

</body>

</html>
```

在浏览器预览效果如下：

分析：

getElementsByName()方法在实际开发中比较少用，大家了解一下即可。

其实可以这样说， getElementById()和getElementsByName()这两种方法是“JavaScript选择器”。

除了getElementById()和getElementsByName()这两种方法，JavaScript还提供另外一种getElementByTagName()方法，这个方法类似于CSS中的元素选择器。但是getElementByTagName()方法有个很大缺点，它会把整个页面中相同的元素都选中。用起来也比较麻烦，实际开发中很少用到。


1、创建节点

在JavaScript中，创建新节点都是先用document对象中的createElement()和createTextNode()这2种方法创建一个元素节点，然后再通过appendChild()、insertBefore()等方法把新元素节点插入现有的元素节点中去。

语法：

```
var e = document.createElement("元素名");

var txt = document.createTextNode("元素内容");

e.appendChild(t);    //把元素内容插入元素中去
```

**2、插入节点**

在JavaScript中，插入节点有2种方法：appendChild()和insertBefore()。

（1）appendChild()

在JavaScript中，我们可以使用appenChild()方法把新的节点插入到当前节点的“内部”。

语法：

```
obj.appendChild(new)
```

说明：

obj表示当前节点，new表示新节点。

（2）insertBefore()

在JavaScript中，我们可以使用insertBefore()方法将新的子节点添加到当前节点的“末尾”。

语法：

```
obj.insertBefore(new,ref)
```

说明：

obj表示父节点；

new表示新的子节点；

ref指定一个节点，在这个节点前插入新的节点。

**3、删除节点**

在JavaScript中，我们可以使用removeChild()方法来删除当前节点下的某个子节点。

语法：

```
obj.removeChild(oldChild);
```

说明：

参数obj表示当前节点，而参数oldChild表示需要当前节点内部的某个子节点。

**4、复制节点**

在JavaScript中，我们可以使用cloneNode()方法来实现复制节点。

语法：

```
obj.cloneNode(bool)
```

说明：

obj，表示被替换节点的父节点；

new，表示替换后的新节点；

old，需要被替换的旧节点。

6、innerHTML和innerText

在JavaScript中，我们可以使用innerHTML和innerText这2个属性很方便地获取和设置某一个元素内部子元素或文本。

innerHTML属性被多数浏览器所支持，而innerText只能被IE、chrome等支持而不被Firefox支持。


**7、JavaScript操作CSS样式**

在JavaScript中，对于元素的CSS操作，我们使用的是DOM对象中的style对象来操作。

语法：

```
obj.style.属性名;
```

说明：

obj指的是DOM对象，也就是通过document.getElementById()等获取而来的DOM元素节点。

6、JavaScript对象
一、事件是什么？
在JavaScript中，事件往往是页面的一些动作引起的，例如当用户按下鼠标或者提交表单，甚至在页面移动鼠标时，事件都会出现。

二、JavaScript事件
在JavaScript中，调用事件的方式共有2种：

（1）在script标签中调用；

在script标签中调用事件，也就是在<script></script标签内部调用事件。

语法：


```
var 变量名 = document.getElementById("元素id");//获取某个元素，并赋值给某个变量

变量名.事件处理器 = function()

{

    ……

}
```

举例：

```html
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <title></title>

</head>

<body>

    <input id="btn" type="button" value="提交" />

    <script type="text/javascript">

        var e = document.getElementById("btn");

        e.onclick = function () {

            alert("绿叶学习网");

        }

    </script>

</body>

</html>
```

在浏览器预览效果如下：

![JavaScript事件调用方式](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzE0LTItMS5wbmc?x-oss-process=image/format,png)

分析：

当点击了按钮之后，JavaScript就会调用鼠标的点击（onclick）事件，效果如下：

![JavaScript调用事件](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzE0LTItMi5wbmc?x-oss-process=image/format,png)

很多人觉得很奇怪，document.getElementById()获取的是一个元素，能赋值给一个变量吗？答案是可以的。那问题又来了，为什么要使用document.getElementById()来获取一个元素赋值给一个变量呢？用以下代码不行么？

```
<script type="text/javascript">

    document.getElementById("btn").onclick = function{

        alert("绿叶学习网");

    }

</script>
```

其实上述代码也是可行的，只不过呢，如果不使用document.getElementById()来获取一个元素赋值给一个变量，以后我们如果要对该元素进行多次不同操作，岂不是每次都要写document.getElementById()？这样的话，代码就会显得很冗余。

（2）在元素中调用；

在元素事件中引入JS，就是指在元素的某一个属性中直接编写JavaScript程序或调用JavaScript函数，这个属性指的是元素的“事件属性”。

举例1：（在元素事件属性中直接编写JavaScript）

```javascript
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <title></title>

</head>

<body>

    <input type="button" onclick="alert('绿叶学习网')" value="按钮"/>

<body>

</html>
```

在浏览器预览效果如下（点击按钮后的效果）：

![在元素中调用JavaScript事件](https://imgconvert.csdnimg.cn/aHR0cDovL3d3dy5sdnllc3R1ZHkuY29tL0FwcF9pbWFnZXMvbGVzc29uL2pzLzE0LTItMy5wbmc?x-oss-process=image/format,png)

举例：

```javascript
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <title></title>

    <script type="text/javascript">

        function alertMessage()

        {

            alert("绿叶学习网");

        }

    </script>

</head>

<body>

    <input type="button" onclick="alertMessage()" value="按钮"/>

<body>

</html>
```

在浏览器预览效果（点击按钮后的效果）如下：



分析：

那么这两种方法有什么本质的区别呢？其实，第2种方法不需要使用getElementById()等方法来获取DOM，然后才调用函数或方法。因为它是直接在JavaScript元素中调用的。

这2种调用JavaScript事件的方式，大家刚刚开始看不理解没关系，我们只是给大家说个语法，留个印象。在接下来的章节中，我们会经常接触。


**1、鼠标事件**

​                                                                               JavaScript鼠标事件

| **事件**    | **说明**     |
| ----------- | ------------ |
| onclick     | 鼠标单击事件 |
| ondbclick   | 鼠标双击事件 |
| onmouseover | 鼠标移入事件 |
| onmouseout  | 鼠标移出事件 |
| onmousemove | 鼠标移动事件 |
| onmousedown | 鼠标按下事件 |
| onmouseup   | 鼠标松开事件 |

**2、键盘事件**

JavaScript键盘事件只有3个：

​                                                                               JavaScript键盘事件

| **方法**   | **说明**                         |
| ---------- | -------------------------------- |
| onkeydown  | 按下键事件（包括数字键、功能键） |
| onkeypress | 按下键事件（只包含数字键）       |
| onkeyup    | 放开键事件（包括数字键、功能键） |

三个事件的执行顺序如下：onkeydown -> onkeypress ->onkeyup。

**3、表单事件**

在JavaScript中，常用的表单事件有4种：

​                                                                             JavaScript鼠标事件

| **事件** | **说明**     |
| -------- | ------------ |
| onfocus  | 获取焦点事件 |
| onblur   | 失去焦点事件 |
| onchange | 状态改变事件 |
| onselect | 选中文本事件 |

**4、编辑事件**

在JavaScript中，常见的编辑事件有3种：

​                                                                          JavaScript编辑事件

| **方法** | **说明** |
| -------- | -------- |
| oncopy   | 复制事件 |
| oncut    | 剪切事件 |
| onpaste  | 粘贴事件 |

这3个事件都对应有一个“onbeforeXXX”事件，表示发生在该事件之前的事件。

**5、页面相关事件**

在JavaScript中，常用的页面相关事件有3种：

​                                                                        JavaScript编辑事件

| **方法** | **说明**               |
| -------- | ---------------------- |
| onload   | 页面加载事件           |
| onresize | 页面大小事件           |
| onerror  | 页面或图片加载出错事件 |

